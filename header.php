<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="//gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php 
$options = get_option( 'theme_option' );
?>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v10.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
        attribution="setup_tool"
        page_id="101905228315598">
      </div>

    <header>
        <div class="container">
            <div class="top-header">
                <div class="logo">
                    <a href="<?php echo get_home_url('/')?>"><img src="<?php echo esc_url($options['logo']);?>" width="230" height="57"
                            alt="Language Link" class="logo"></a>
                </div>
                <div class="menu-top">
                    <?php
                        $primarymenu = array(
                            'theme_location'  => 'top',
                            'menu'            => '',
                            'container'       => '',
                            'container_class' => '',
                            'container_id'    => '',
                            'menu_class'      => 'top-menu',
                            'menu_id'         => '',
                            'echo'            => true,
                            'before'          => '',
                            'after'           => '',
                            'link_before'     => '',
                            'link_after'      => '',
                            'items_wrap'      => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
                            'depth'           => 0,
                            'walker' => new Walker_Nav_Menu_Custom()
                            );
                            if ( has_nav_menu( 'primary' ) ) {
                            wp_nav_menu( $primarymenu );
                        }
                    ?>
                </div>
                <div class="right-top">
                    <span class="hotline">Hotline</span>
                    <span class="hotline_number"><a href="tel:1900633683" style="color:#fff"><?php echo esc_html($options['phone-number']);?></a></span>
                </div>
            </div>
            <div id="main-menu">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <?php
                            $primarymenu = array(
                                'theme_location'  => 'primary',
                                'menu'            => '',
                                'container'       => '',
                                'container_class' => '',
                                'container_id'    => '',
                                'menu_class'      => 'navbar-nav mr-auto',
                                'menu_id'         => '',
                                'echo'            => true,
                                'before'          => '',
                                'after'           => '',
                                'link_before'     => '',
                                'link_after'      => '',
                                'items_wrap'      => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
                                'depth'           => 0,
                                'walker' => new Walker_Nav_Menu_Custom()
                                );
                                if ( has_nav_menu( 'primary' ) ) {
                                wp_nav_menu( $primarymenu );
                            }
                        ?>
                    </div>
                </nav>
            </div>
        </div>
    </header>
<?php
/*
Template Name: Contact
*/
get_header();
if(have_posts()) : the_post();
$url_image_large = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID(),'full') );
$theme_option = get_option('theme_option');
$mtr_title_contact = isset($theme_option['mtr_title_contact']) ? $theme_option['mtr_title_contact'] : "";
$mtr_address_contact = isset($theme_option['mtr_address_contact']) ? $theme_option['mtr_address_contact'] : "";
$mtr_hotline_contact = isset($theme_option['mtr_hotline_contact']) ? $theme_option['mtr_hotline_contact'] : "";
$mtr_hotline_contact_kd = isset($theme_option['mtr_hotline_contact_kd']) ? $theme_option['mtr_hotline_contact_kd'] : "";
$mtr_email_contact = isset($theme_option['mtr_email_contact']) ? $theme_option['mtr_email_contact'] : "";
$mtr_website_contact = isset($theme_option['mtr_website_contact']) ? $theme_option['mtr_website_contact'] : "";
$mtr_link_map_contact = isset($theme_option['mtr_link_map_contact']) ? $theme_option['mtr_link_map_contact'] : "";
$sitekey_captcha = isset($theme_option['mtr_sitekey_captcha']) ? $theme_option['mtr_sitekey_captcha'] : "6LeuetYUAAAAACmHI4qoCThK0Rc3Yz1yECC4b9wz";
?>
<section class="banner  main-section" style="background-image:url('<?php echo esc_url($url_image_large);?>');">
    <div class="container">
        <div class="row">
            <div class="section-part text-center">
                <h3 class="text-left text-white font-size-30">
                    <?php the_title();?>
                </h3>
            </div>
        </div>
    </div>
</section>
<section class="container full-width margin-top-40">
    <div class="row">
        <div class="col-12">
            <h3 class="text-center color-primary font-size-30">
                <?php echo esc_html($mtr_title_contact);?>
            </h3>
        </div>
    </div>
</section>
<section class="content-page container full-width margin-top-40 ">
    <div class="row">
        <div class="col-sm-12 col-12">
            <div class="content-contact">
                <p class="text-center"><?php echo esc_html($mtr_address_contact);?></p>
                <div class="infor-contact d-sm-flex justify-content-between">

                    <div class="infor-left d-inline-block text-left">
                        <p><?php echo esc_html($mtr_hotline_contact);?></p>
                        <p><?php echo esc_html($mtr_hotline_contact_kd);?></p>
                    </div>
                    <div class="infor-left d-inline-block text-right">
                        <p><?php echo esc_html($mtr_email_contact);?> </p>
                        <p><?php echo esc_html($mtr_website_contact);?>  </p>
                    </div>
                </div>
            </div>                 
        </div>
        <div class="col-12">
            <div class="button-link text-center ">
                <a href="<?php echo esc_url($mtr_link_map_contact);?>" class="btn-link-map btn btn-primary btn-1" target="_blank">Xem bản đồ</a>
            </div>
            
        </div>
        <div class="form-contact_ccv tth-contact-form margin-top-40">
            <form action="" class="">
                <div class="row">
                    <div class="col-sm-6 col-12">
                        <div class="form-group">
                            <label for="basic-url">Họ và tên <span>*</span></label>
                            <input type="text" class="form-control" id="fullname" name="fullname" required>
                        </div>
                    </div>
                    <div class="col-sm-6 col-12">
                        <div class="form-group">
                            <label for="basic-url">Tiêu đề <span>*</span></label>
                            <input type="text" class="form-control" id="subtitle" name="subtitle" required>
                        </div>
                    </div>
                    <div class="col-sm-6 col-12">
                        <div class="form-group">
                            <label for="basic-url">Điện thoại <span>*</span></label>
                            <input type="text" class="form-control" id="phone" name="phone" required>
                        </div>
                        <div class="form-group">
                            <label for="basic-url">Email <span>*</span></label>
                            <input type="text" class="form-control" name="mail" id="mail" required>
                        </div>
                    </div>
                    <div class="col-sm-6 col-12">
                        <div class="form-group">
                            <label for="basic-url">Nội dung <span>*</span></label>
                            <textarea name="content" id="content" id="content"></textarea>
                        </div>
                    </div>
                    <div class="col-sm-6 col-12">
                        <div class="form-group">
                            <label for="basic-url">Địa chỉ <span>*</span></label>
                            <input type="text" class="form-control" name="address" id="address" required>
                        </div>
                    </div>
                    <div class="col-sm-6 col-12">
                        <div class="g-recaptcha" data-sitekey="<?php echo esc_attr($sitekey_captcha);?>"></div>
                    </div>
                    <div class="col-sm-12 col-12 text-center">
                        <button class="button-ajax-sent-mail btn btn-primary btn-1 d-flex justify-content-center align-items-center align-self-cente">
                            <div class="text-button">GỬI</div>
                            <div class="tth-loader-new tth-none"><i class="lni-spinner fas fa-spinner"></i></div>
                        </button>
                    </div>
                    <div class="form-group col-md-12">
                        <p class="tth-message-res"></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<?php
endif;
get_footer();?>
          
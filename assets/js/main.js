var mySwiper = new Swiper('.swiper-container-slider-video', {
  slidesPerView: 3,
  spaceBetween: 5,
  freeMode: true,
  pagination: {
      el: '.ccv-swiper-pagination',
      clickable: true,
  },

  // Nếu cần navigation
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});
function checkphone() {
    var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    var mobile = $("#txtphone-fix").val();
    if (vnf_regex.test(mobile) == true && mobile.length == 10) {
        submitted = true;
        $("#btn-fix-register").disabled = true;
        $("#btn-fix-register").css("display", "none");
        $(".fix-sidebar-2-submit #loading").css("display", "flex");
    } else {
        alert("Số điện thoại của bạn không đúng định dạng!");
        $("#txtphone-fix").focus();
        return false;
    }
}
if(typeof(jQuery) !== 'undefined') {
$ = jQuery.noConflict();
}
$('#goto').on('click', function() {  
window.open($('#branch option:selected').val(), "_blank");
});

jQuery(function($){
  var owl = $(".container-slider-video");
  owl.owlCarousel({
      loop: true,
      autoplay: false,
      items: 3,
      margin: 30,
      autoHeight: true,
      responsiveClass: true,
      autoplayHoverPause: true,
      dots: true,
      nav: false,
      responsive: {
          0: {
              items: 1,
          },
          600: {
              items: 2,
          },
          1000: {
              items: 3,
          },
      },
  });

  var partner = $(".container-slider-partner");
  partner.owlCarousel({
      loop: true,
      autoplay: false,
      items: 6,
      margin: 30,
      autoHeight: true,
      responsiveClass: true,
      autoplayHoverPause: true,
      dots: false,
      nav: false,
      responsive: {
          0: {
              items: 1,
          },
          600: {
              items: 2,
          },
          1000: {
              items: 6,
          },
      },
  });
})


var youtube = document.querySelectorAll( ".youtube" );
for (var i = 0; i < youtube.length; i++) {			
var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/hqdefault.jpg";			
var image = new Image();
image.src = source;
image.addEventListener( "load", function() {
youtube[ i ].appendChild( image );
}( i ) );			
youtube[i].addEventListener( "click", function() {
var iframe = document.createElement( "iframe" );
iframe.setAttribute( "frameborder", "0" );
iframe.setAttribute( "allowfullscreen", "" );
iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );
this.innerHTML = "";
this.appendChild( iframe );								
} );    
};

//tuvan
if (typeof jQuery !== "undefined") {
  $ = jQuery.noConflict();
}
function change(el) {
  var $selected = $(el).val();
  var $huyen1 =
      '<option value="Ba Đình" style="background-color: #fff; color: #000">Ba Đình</option>' +
      '<option value="Bắc Từ Liêm" style="background-color: #fff; color: #000">Bắc Từ Liêm</option>' +
      '<option value="Cầu Giấy" style="background-color: #fff; color: #000">Cầu Giấy</option>' +
      '<option value="Chương Mỹ" style="background-color: #fff; color: #000">Chương Mỹ</option>' +
      '<option value="Đan Phượng" style="background-color: #fff; color: #000">Đan Phượng</option>' +
      '<option value="Đông Anh" style="background-color: #fff; color: #000">Đông Anh</option>' +
      '<option value="Đống Đa" style="background-color: #fff; color: #000">Đống Đa</option>' +
      '<option value="Gia Lâm" style="background-color: #fff; color: #000">Gia Lâm</option>' +
      '<option value="Hà Đông" style="background-color: #fff; color: #000">Hà Đông</option>' +
      '<option value="Hai Bà Trưng" style="background-color: #fff; color: #000">Hai Bà Trưng</option>' +
      '<option value="Hoài Đức" style="background-color: #fff; color: #000">Hoài Đức</option>' +
      '<option value="Hoàn Kiếm" style="background-color: #fff; color: #000">Hoàn Kiếm</option>' +
      '<option value="Hoàng Mai" style="background-color: #fff; color: #000">Hoàng Mai</option>' +
      '<option value="Long Biên" style="background-color: #fff; color: #000">Long Biên</option>' +
      '<option value="Mê Linh" style="background-color: #fff; color: #000">Mê Linh</option>' +
      '<option value="Nam Từ Liêm" style="background-color: #fff; color: #000">Nam Từ Liêm</option>' +
      '<option value="Quốc Oai" style="background-color: #fff; color: #000">Quốc Oai</option>' +
      '<option value="Tây Hồ" style="background-color: #fff; color: #000">Tây Hồ</option>' +
      '<option value="Thanh Oai" style="background-color: #fff; color: #000">Thanh Oai</option>' +
      '<option value="Thanh Trì" style="background-color: #fff; color: #000">Thanh Trì</option>' +
      '<option value="Thanh Xuân" style="background-color: #fff; color: #000">Thanh Xuân</option>' +
      '<option value="Thường Tín" style="background-color: #fff; color: #000">Thường Tín</option>' +
      '<option value="Khác" style="background-color: #fff; color: #000">Khác</option>';
  var $huyen2 =
      '<option value="Bạch Đằng" style="background-color: #fff; color: #000">Bạch Đằng</option>' +
      '<option value="Bãi Cháy" style="background-color: #fff; color: #000">Bãi Cháy</option>' +
      '<option value="Cao Thắng" style="background-color: #fff; color: #000">Cao Thắng</option>' +
      '<option value="Cao Xanh" style="background-color: #fff; color: #000">Cao Xanh</option>' +
      '<option value="Đại Yên" style="background-color: #fff; color: #000">Đại Yên</option>' +
      '<option value="Giếng Đáy" style="background-color: #fff; color: #000">Giếng Đáy</option>' +
      '<option value="Hà Khánh" style="background-color: #fff; color: #000">Hà Khánh</option>' +
      '<option value="Hà Khẩu" style="background-color: #fff; color: #000">Hà Khẩu</option>' +
      '<option value="Hà Lầm" style="background-color: #fff; color: #000">Hà Lầm</option>' +
      '<option value="Hà Phong" style="background-color: #fff; color: #000">Hà Phong</option>' +
      '<option value="Hà Trung" style="background-color: #fff; color: #000">Hà Trung</option>' +
      '<option value="Hà Tu" style="background-color: #fff; color: #000">Hà Tu</option>' +
      '<option value="Hồng Gai" style="background-color: #fff; color: #000">Hồng Gai</option>' +
      '<option value="Hồng Hà" style="background-color: #fff; color: #000">Hồng Hà</option>' +
      '<option value="Hồng Hải" style="background-color: #fff; color: #000">Hồng Hải</option>' +
      '<option value="Hùng Thắng" style="background-color: #fff; color: #000">Hùng Thắng</option>' +
      '<option value="Trần Hưng Đạo" style="background-color: #fff; color: #000">Trần Hưng Đạo</option>' +
      '<option value="Tuần Châu" style="background-color: #fff; color: #000">Tuần Châu</option>' +
      '<option value="Việt Hưng" style="background-color: #fff; color: #000">Việt Hưng</option>' +
      '<option value="Yết Kiêu" style="background-color: #fff; color: #000">Yết Kiêu</option>' +
      '<option value="Khác" style="background-color: #fff; color: #000">Khác</option>';
  var $huyen3 =
      '<option value="An Hoạch" style="background-color: #fff; color: #000">An Hoạch</option>' +
      '<option value="Ba Đình" style="background-color: #fff; color: #000">Ba Đình</option>' +
      '<option value="Điện Biên" style="background-color: #fff; color: #000">Điện Biên</option>' +
      '<option value="Đông Cương" style="background-color: #fff; color: #000">Đông Cương</option>' +
      '<option value="Đông Hải" style="background-color: #fff; color: #000">Đông Hải</option>' +
      '<option value="Đông Hương" style="background-color: #fff; color: #000">Đông Hương</option>' +
      '<option value="Đông Sơn" style="background-color: #fff; color: #000">Đông Sơn</option>' +
      '<option value="Đông Thọ" style="background-color: #fff; color: #000">Đông Thọ</option>' +
      '<option value="Đông Vệ" style="background-color: #fff; color: #000">Đông Vệ</option>' +
      '<option value="Hàm Rồng" style="background-color: #fff; color: #000">Hàm Rồng</option>' +
      '<option value="Lam Sơn" style="background-color: #fff; color: #000">Lam Sơn</option>' +
      '<option value="Nam Ngạn" style="background-color: #fff; color: #000">Nam Ngạn</option>' +
      '<option value="Ngọc Trạo" style="background-color: #fff; color: #000">Ngọc Trạo</option>' +
      '<option value="Phú Sơn" style="background-color: #fff; color: #000">Phú Sơn</option>' +
      '<option value="Quảng Hưng" style="background-color: #fff; color: #000">Quảng Hưng</option>' +
      '<option value="Quảng Thành" style="background-color: #fff; color: #000">Quảng Thành</option>' +
      '<option value="Quảng Thắng" style="background-color: #fff; color: #000">Quảng Thắng</option>' +
      '<option value="Tào Xuyên" style="background-color: #fff; color: #000">Tào Xuyên</option>' +
      '<option value="Tân Sơn" style="background-color: #fff; color: #000">Tân Sơn</option>' +
      '<option value="Trường Thi" style="background-color: #fff; color: #000">Trường Thi</option>' +
      '<option value="Khác" style="background-color: #fff; color: #000">Khác</option>';
  var $huyen4 =
      '<option value="Mỹ Hào" style="background-color: #fff; color: #000">Mỹ Hào</option>' +
      '<option value="Ân Thi" style="background-color: #fff; color: #000">Ân Thi</option>' +
      '<option value="Khoái Châu" style="background-color: #fff; color: #000">Khoái Châu</option>' +
      '<option value="Kim Động" style="background-color: #fff; color: #000">Kim Động</option>' +
      '<option value="Phù Cừ" style="background-color: #fff; color: #000">Phù Cừ</option>' +
      '<option value="Tiên Lữ" style="background-color: #fff; color: #000">Tiên Lữ</option>' +
      '<option value="Văn Giang" style="background-color: #fff; color: #000">Văn Giang</option>' +
      '<option value="Văn Lâm" style="background-color: #fff; color: #000">Văn Lâm</option>' +
      '<option value="Yên Mỹ" style="background-color: #fff; color: #000">Yên Mỹ</option>' +
      '<option value="Khác" style="background-color: #fff; color: #000">Khác</option>';
  var $huyen5 =
      '<option value="Bình Giang" style="background-color: #fff; color: #000">Bình Giang</option>' +
      '<option value="Cẩm Giàng" style="background-color: #fff; color: #000">Cẩm Giàng</option>' +
      '<option value="Gia Lộc" style="background-color: #fff; color: #000">Gia Lộc</option>' +
      '<option value="Kim Thành" style="background-color: #fff; color: #000">Kim Thành</option>' +
      '<option value="Kinh Môn" style="background-color: #fff; color: #000">Kinh Môn</option>' +
      '<option value="Nam Sách" style="background-color: #fff; color: #000">Nam Sách</option>' +
      '<option value="Ninh Giang" style="background-color: #fff; color: #000">Ninh Giang</option>' +
      '<option value="Thanh Hà" style="background-color: #fff; color: #000">Thanh Hà</option>' +
      '<option value="Thanh Miện" style="background-color: #fff; color: #000">Thanh Miện</option>' +
      '<option value="Tứ Kỳ" style="background-color: #fff; color: #000">Tứ Kỳ</option>' +
      '<option value="Khác" style="background-color: #fff; color: #000">Khác</option>';
  var $huyen6 =
      '<option value="Cẩm Lệ" style="background-color: #fff; color: #000">Cẩm Lệ</option>' +
      '<option value="Hải Châu" style="background-color: #fff; color: #000">Hải Châu</option>' +
      '<option value="Liên Chiểu" style="background-color: #fff; color: #000">Liên Chiểu</option>' +
      '<option value="Ngũ Hành Sơn" style="background-color: #fff; color: #000">Ngũ Hành Sơn</option>' +
      '<option value="Sơn Trà" style="background-color: #fff; color: #000">Sơn Trà</option>' +
      '<option value="Thanh Khê" style="background-color: #fff; color: #000">Thanh Khê</option>' +
      '<option value="Hòa Vang" style="background-color: #fff; color: #000">Hòa Vang</option>' +
      '<option value="Hoàng Sa" style="background-color: #fff; color: #000">Hoàng Sa</option>' +
      '<option value="Khác" style="background-color: #fff; color: #000">Khác</option>';
  var $huyen7 =
      '<option value="Phường Minh Khai" style="background-color: #fff; color: #000">Phường Minh Khai</option>' +
      '<option value="Phường Quang Trung" style="background-color: #fff; color: #000">Phường Quang Trung</option>' +
      '<option value="Phường Trần Phú" style="background-color: #fff; color: #000">Phường Trần Phú</option>' +
      '<option value="Phường Nguyễn Trãi" style="background-color: #fff; color: #000">Phường Nguyễn Trãi</option>' +
      '<option value="Phường Ngọc Hà" style="background-color: #fff; color: #000">Phường Ngọc Hà</option>' +
      '<option value="Xã Ngọc Đường" style="background-color: #fff; color: #000">Xã Ngọc Đường</option>' +
      '<option value="Xã Phương Độ" style="background-color: #fff; color: #000">Xã Phương Độ</option>' +
      '<option value="Xã Phương Thiện" style="background-color: #fff; color: #000">Xã Phương Thiện</option>' +
      '<option value="Khác" style="background-color: #fff; color: #000">Khác</option>';
  var $huyen8 =
      '<option value="Thị xã Từ Sơn" style="background-color: #fff; color: #000">Thị xã Từ Sơn</option>' +
      '<option value="Huyện Gia Bình" style="background-color: #fff; color: #000">Huyện Gia Bình</option>' +
      '<option value="Huyện Lương Tài" style="background-color: #fff; color: #000">Huyện Lương Tài</option>' +
      '<option value="Huyện Quế Võ" style="background-color: #fff; color: #000">Huyện Quế Võ</option>' +
      '<option value="Huyện Thuận Thành" style="background-color: #fff; color: #000">Huyện Thuận Thành</option>' +
      '<option value="Huyện Tiên Du" style="background-color: #fff; color: #000">Huyện Tiên Du</option>' +
      '<option value="Huyện Yên Phong" style="background-color: #fff; color: #000">Huyện Yên Phong</option>';
  var $huyen9 =
      '<option value="Phường Bạch Hạc" style="background-color: #fff; color: #000">Phường Bạch Hạc</option>' +
      '<option value="Phường Bến Gót" style="background-color: #fff; color: #000">Phường Bến Gót</option>' +
      '<option value="Phường Dữu Lâu" style="background-color: #fff; color: #000">Phường Dữu Lâu</option>' +
      '<option value="Phường Gia Cẩm" style="background-color: #fff; color: #000">Phường Gia Cẩm</option>' +
      '<option value="Phường Minh Nông" style="background-color: #fff; color: #000">Phường Minh Nông</option>' +
      '<option value="Phường Minh Phương" style="background-color: #fff; color: #000">Phường Minh Phương</option>' +
      '<option value="Phường Nông Trang" style="background-color: #fff; color: #000">Phường Nông Trang</option>' +
      '<option value="Phường Tân Dân" style="background-color: #fff; color: #000">Phường Tân Dân</option>' +
      '<option value="Phường Thanh Miếu" style="background-color: #fff; color: #000">Phường Thanh Miếu</option>' +
      '<option value="Phường Thọ Sơn" style="background-color: #fff; color: #000">Phường Thọ Sơn</option>' +
      '<option value="Phường Tiên Cát" style="background-color: #fff; color: #000">Phường Tiên Cát</option>' +
      '<option value="Phường Vân Cơ" style="background-color: #fff; color: #000">Phường Vân Cơ</option>' +
      '<option value="Phường Vân Phú" style="background-color: #fff; color: #000">Phường Vân Phú</option>';
  switch ($selected) {
      case "Hạ Long":
          $("#ddlFruits3").html($huyen2);
          break;
      case "Thanh Hóa":
          $("#ddlFruits3").html($huyen3);
          break;
      case "Hưng Yên":
          $("#ddlFruits3").html($huyen4);
          break;
      case "Hải Dương":
          $("#ddlFruits3").html($huyen5);
          break;
      case "Đà Nẵng":
          $("#ddlFruits3").html($huyen6);
          break;
      case "Hà Giang":
          $("#ddlFruits3").html($huyen7);
          break;
      case "Bắc Ninh":
          $("#ddlFruits3").html($huyen8);
          break;
      case "Phú Thọ":
          $("#ddlFruits3").html($huyen9);
          break;
      default:
          $("#ddlFruits3").html($huyen1);
          break;
  }
}
function Validate() {
  var ddlFruits1 = document.getElementById("ddlFruits1");
  var ddlFruits2 = document.getElementById("ddlFruits2");
  var ddlFruits3 = document.getElementById("ddlFruits3");
  var ddlFruits4 = document.getElementById("ddlFruits4");
  if (ddlFruits1.value == "" || ddlFruits2.value == "" || ddlFruits3.value == "" || ddlFruits4.value == "") {
      //If the "Please Select" option is selected display error.
      alert("Vui lòng điền đầy đủ thông tin");
      return false;
  }
  return true;
}
$("#mc-form").submit(function (event) {
  $("#btnsubmit").text("Đang gửi...");
});

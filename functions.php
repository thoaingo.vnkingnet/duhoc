<?php 

if (  file_exists( dirname( __FILE__ ) . '/includes/plugin/codestar-framework/codestar-framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/includes/plugin/codestar-framework/codestar-framework.php' );
}

require get_template_directory()  . '/includes/custom-walker-nav-menu.php' ;
require get_template_directory()  . '/includes/register-posttype.php' ;
require get_template_directory()  . '/includes/widget/RecentNews.php' ;
function ccv_theme_setup() {
    add_theme_support( 'custom-header' ); 
    add_theme_support( 'custom-background' );
    add_theme_support ('title-tag');
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'post-formats', array(
        'video',
        'gallery',
        'image'
    ) );
    $defaults = array(
        'height' => 230,
        'width' => 57,
        'flex-height' => true,
        'flex-width' => true,
        'header-text' => array(),
    );

    add_theme_support( 'custom-logo' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list','gallery', ) );
    add_post_type_support( 'golf', array( 'comments' ) );
    register_nav_menus( array(
        'primary' => esc_html__('Primary Navigation Menu (Use For All Page)','matara'),   
        
    ) );
    register_nav_menus( array(
        'footer' => esc_html__('Menu Footer','matara'),   
        
    ) );
    register_nav_menus( array(
        'top' => esc_html__('Menu Top','matara'),   
        
    ) );
}
add_action( 'after_setup_theme', 'ccv_theme_setup' );
function chengchivas_load_theme_textdomain() {
    load_theme_textdomain( 'matara', get_template_directory_uri() . '/languages/' );
}
add_action( 'after_setup_theme', 'chengchivas_load_theme_textdomain' );
function chengchivas_theme_scripts_styles(){
    /**** Theme Specific CSS ****/
    $protocol = is_ssl() ? 'https' : 'http';
    wp_enqueue_script('jquery');
    wp_enqueue_style( 'family-fonts-Montserrat', 'https://fonts.googleapis.com/css?family=Montserrat', array(), '1.0.0' );
    wp_enqueue_style( 'family-fonts-Roboto', 'https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap' );
    wp_enqueue_style( 'owl.carousel.css', get_template_directory_uri().'/assets/css/owl.carousel.min.css', array(), true,false );
    wp_enqueue_style( 'owl.theme.default.css', get_template_directory_uri().'/assets/css/owl.theme.default.min.css', array(), true,false );
    wp_enqueue_style( 'magnific-popup.css', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css', array(), true,false );
    wp_enqueue_style( 'bootstrap.css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', array(), true,false );
    wp_enqueue_style( 'swiper-bundle', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.15/swiper-bundle.min.css', array(), true,false );

    wp_enqueue_style( 'custom_style.css', get_template_directory_uri().'/assets/css/main.css', array(), true,false );
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    /**** Start Jquery ****/ 
    wp_enqueue_script("fontawesome.min", "https://kit.fontawesome.com/35aeac332b.js",array(),true,false);
    wp_enqueue_script("recaptcha.js", "https://www.google.com/recaptcha/api.js?ver=1",array(),true,false);
    wp_enqueue_script("wow.min", "https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js",array(),false,true);
    wp_enqueue_script("popper.min", "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js",array(),false,true);
    wp_enqueue_script("magnific-popup.", "https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js",array(),false,true);
        wp_enqueue_script("bootstrap-js.", "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js",array(),false,true);
    wp_enqueue_script("swiper-bundle.", "https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.15/swiper-bundle.js",array(),false,true);
    
    wp_enqueue_script("owl.carousel.js", get_template_directory_uri()."/assets/js/owl.carousel.js",array(),false,true);
    
    wp_enqueue_script("main.js", get_template_directory_uri()."/assets/js/main.js",array(),false,true);
    wp_localize_script('jquery', 'chengchivas_params', [
        'theme_url' => get_template_directory_uri(),
        'site_url' => site_url(),
        'ajax_url' => admin_url('admin-ajax.php'),
    ]);
}
add_action( 'wp_enqueue_scripts', 'chengchivas_theme_scripts_styles' );

function ccv_add_sidebar() {
    register_sidebar([
        'name' => __('Blog Sidebar', 'chengchivas'),
        'id' => 'blog-sidebar',
        'description' => __('Widgets in this area will be shown on footer.', 'chengchivas'),
        'before_title' => '<h3 class="title text-left text-uppercase color-primary>',
        'after_title' => '</h3>',
        'before_widget' => '<div id="%1$s" class="item-sidebar %2$s">',
        'after_widget' => '</div>',
    ]);
    
}
function mtr_load_template($slug, $name = false, $data = array())
    {
        $template_dir = 'views';
        if (is_array($data))
            extract($data);

        if ($name) {
            $slug = $slug . '-' . $name;
        }
        $template = locate_template($template_dir . '/' . $slug . '.php');
        if (is_file($template)) {
            ob_start();
            include $template;
            $data = @ob_get_clean();

            return $data;
        }
}

if (!function_exists('chengchivas_add_sidebar')) {

    function chengchivas_add_sidebar() {
        register_sidebar([
            'name' => __('Blog Sidebar', 'matara'),
            'id' => 'blog-sidebar',
            'description' => __('Widgets in this area will be shown on all posts and pages.', 'matara'),
            'before_title' => '<h4>',
            'after_title' => '</h4>',
            'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
            'after_widget' => '</div>',
        ]);
        register_sidebar([
            'name' => __('Footer 1', 'matara'),
            'id' => 'footer-one',
            'description' => __('Widgets in this area will be shown on all posts and pages.', 'matara'),
            'before_title' => '<h3>',
            'after_title' => '</h3><hr>',
            'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
            'after_widget' => '</div>',
        ]);
        register_sidebar([
            'name' => __('Footer 2', 'matara'),
            'id' => 'footer-two',
            'description' => __('Widgets in this area will be shown on all posts and pages.', 'matara'),
            'before_title' => '<h3>',
            'after_title' => '</h3><hr>',
            'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
            'after_widget' => '</div>',
        ]);
        register_sidebar([
            'name' => __('Footer 3', 'matara'),
            'id' => 'footer-three',
            'description' => __('Widgets in this area will be shown on all posts and pages.', 'matara'),
            'before_title' => '<h3>',
            'after_title' => '</h3><hr>',
            'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
            'after_widget' => '</div>',
        ]);
        
    }
}
add_action( 'widgets_init', 'chengchivas_add_sidebar' );
function chengchivas_select_posttype($post_type){
    $term = isset($_POST['query']) ? esc_attr($_POST['query']) : "";
    $get_post = get_posts( array( 'post_type'=> $post_type, 'posts_per_page' =>-1, 'post_status' => 'publish' ) );
    $sliders = [];
    if(!empty($get_post)){
        foreach ($get_post as $k => $v){
            $sliders[$v->ID] = $v->post_title;
        }
    }
    return $sliders;
}
function chengchivas_get_slider(){ 
    $theme_option = get_option('theme_option');
    $mtr_sliders = isset($theme_option['chengchivas_slider']) ? $theme_option['chengchivas_slider'] : "";
    $get_sliders = get_posts( array( 'post_type'=> 'cv_slider', 'posts_per_page' =>-1, 'post_status' => 'publish', 'post__in' =>($mtr_sliders),'orderby'=> 'post__in' ) );
    if(!empty($get_sliders)){ 
        ?>
        <section class="container-fluid full-width slider">
            <div class="row">
                <div class="owl-carousel owl-theme">
                <?php foreach ($get_sliders as $k => $sl){
                    $url_image = wp_get_attachment_url( get_post_thumbnail_id($sl->ID,'full') );
                    ?>
                    <div class="item">
                        <img class="d-block img-fluid" title="<?php echo esc_attr($sl->post_title);?>" src="<?php echo esc_url($url_image);?>" alt="">
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>
    
    <?php }
}
function chengchivas_get_about_home(){
    $theme_option = get_option('theme_option');
    $chengchivas_about_title = isset($theme_option['chengchivas_about_title']) ? $theme_option['chengchivas_about_title'] : "GIỚI THIỆU <strong>MATARA PHARMA</strong>";
    $chengchivas_about_content = isset($theme_option['chengchivas_about_content']) ? $theme_option['chengchivas_about_content'] : "<p>
                        Giới thiêu về công ty và triết lý kinh doanh. Ví dụ Matara Pharma là đơn vị nghiên cứu, phát triển song song với chuyển giao kỹ thuật, cung ứng nguyên liệu các loại dược mỹ phẩm chất lượng cao, chủng loại sản phẩm đa dạng phong phú, kể cả những sản phẩm chuyên biệt kháng corticoid. Với đội ngũ chuyên viên, nhân viên có kinh nghiệm và trình độ chuyên môn cao cùng dịch vụ chăm sóc khách hàng chuyên nghiệp, Matara Pharma sẽ mang đến sự hài lòng cho quý khách hàng.
                    </p>";
    $chengchivas_about_page = isset($theme_option['chengchivas_about_page']) ? $theme_option['chengchivas_about_page'] : "";
    $chengchivas_about_button = isset($theme_option['chengchivas_about_button']) ? $theme_option['chengchivas_about_button'] : "";
    if(!empty($chengchivas_about_page)){
        $url_page = get_the_permalink($chengchivas_about_page);
    } else {
        $url_page = "#";
    }
    ?>
    <section class="container full-width margin-top-40">
        <div class="row">
            <div class="col-12">
                <h3 class="text-center color-primary">
                    <?php echo htmlspecialchars_decode($chengchivas_about_title);?>
                </h3>
                <div class="content-about">
                    <?php echo htmlspecialchars_decode($chengchivas_about_content);?>
                </div>
            </div>
            <div class="col-12">
                <div class="button-link text-center ">
                    <a href="<?php echo esc_url($url_page);?>" class="btn btn-primary btn-1"><?php echo esc_html($chengchivas_about_button);?></a>
                </div>
                
            </div>
        </div>
    </section>
<?php }

function chengchivas_product_feature_loop(){
    $theme_option = get_option('theme_option');
    $chengchivas_product_feature_loop = isset($theme_option['chengchivas_product_feature_loop']) ? $theme_option['chengchivas_product_feature_loop'] : "";
    if(!empty($chengchivas_product_feature_loop)){ ?>
        <section class="container full-width margin-top-40">
            <div class="row">
                <?php foreach ($chengchivas_product_feature_loop as $key => $box) { ?>
                    <div class="col-sm-4 col-12 item-box">
                        <a class="item-box-link" href="<?php echo esc_url($box['url']);?>">
                            <img class="img-fluid" src="<?php echo esc_url($box['image']);?>" alt="<?php echo esc_attr($box['title']);?>">
                        </a>
                    </div>
                <?php }?>
                
                
            </div>
        </section>
    <?php }
}

function chengchivas_product_feature(){ 
    $theme_option = get_option('theme_option');
    $chengchivas_about_feature_title = isset($theme_option['chengchivas_about_feature_title']) ? $theme_option['chengchivas_about_feature_title'] : "";
    $chengchivas_about_feature_content = isset($theme_option['chengchivas_about_feature_content']) ? $theme_option['chengchivas_about_feature_content'] : "";
    $chengchivas_about_page_timhieu = isset($theme_option['chengchivas_about_page_timhieu']) ? $theme_option['chengchivas_about_page_timhieu'] : "";
    $chengchivas_about_product_button = isset($theme_option['chengchivas_about_product_button']) ? $theme_option['chengchivas_about_product_button'] : "";
    $chengchivas_product_feature_loop_new = isset($theme_option['chengchivas_product_feature_loop_new']) ? $theme_option['chengchivas_product_feature_loop_new'] : "";
    if(!empty($chengchivas_about_page_timhieu)){
        $url_page = get_the_permalink($chengchivas_about_page_timhieu);
    } else {
        $url_page = "#";
    }
    ?>
    <section class="container full-width margin-top-40">
            <div class="row">
                <div class="col-12">
                    <h3 class="text-center color-primary">
                        <?php echo htmlspecialchars_decode($chengchivas_about_feature_title);?>
                    </h3>
                    <div class="content-about">
                        <?php echo htmlspecialchars_decode($chengchivas_about_feature_content);?>
                    </div>
                </div>
                <?php if(!empty($chengchivas_product_feature_loop_new)){ 
                foreach ($chengchivas_product_feature_loop_new as $key => $cate) { ?>
                <div class="col-sm-4 col-12">
                    <div class="item-box-product <?php if($key==1){ echo 'bg-pink';} elseif($key==2){ echo 'bg-brown';} else {}?>">
                        <img class="img-fluid" src="<?php echo esc_url($cate['image']);?>" alt="<?php echo esc_attr($cate['title']);?>">
                        <div class="button-name">
                            <a href="<?php echo esc_url($cate['url']);?>" class="btn btn-primary text-white"><?php echo esc_attr($cate['title']);?></a>
                        </div>
                    </div>
                </div>
                <?php }
                } ?>
                <div class="col-12">
                    <div class="button-link text-center marrgin-top-36">
                        <a href="<?php echo esc_url($url_page);?>" class="btn btn-primary btn-1"><?php echo esc_html($chengchivas_about_product_button);?></a>
                    </div>
                    
                </div>
            </div>
        </section>
<?php }
function chengchivas_new_post_home(){
    $theme_option = get_option('theme_option');
    $chengchivas_new_posts = isset($theme_option['chengchivas_new_posts']) ? $theme_option['chengchivas_new_posts'] : array();
    $chengchivas_new_title_home = isset($theme_option['chengchivas_new_title_home']) ? $theme_option['chengchivas_new_title_home'] : "";
    //$get_posts = get_posts( array( 'post_type'=> 'post', 'posts_per_page' =>-1, 'post_status' => 'publish', 'post__in' => ($chengchivas_new_posts),'orderby'=> 'post__in' ) ); 
    $get_posts = get_posts( array( 'post_type'=> 'post', 'posts_per_page' =>5, 'post_status' => 'publish','orderby'=> 'DESC' ) ); 
    ?>
    <section class="container full-width margin-top-40">
        <div class="row">
            <div class="col-12">
                <h3 class="text-center color-primary">
                    <?php echo esc_html($chengchivas_new_title_home);?>
                </h3>
                
            </div>
        </div>
        <?php if(!empty($get_posts)){
            $url_image_large = wp_get_attachment_url( get_post_thumbnail_id($get_posts[0]->ID,'full') );
            $title_large = $get_posts[0]->post_title;
            $title_des = $get_posts[0]->post_content;
            $words = 75;
            $more = ' … <i class="fas fa-forward"></i>';
            
            $excerpt_title = wp_trim_words( $title_des, $words, $more );

            ?>
            <div class="row  margin-top-40">
                <div class="col-sm-6 col-12">
                    <div class="img-blog img-blog-large">
                        <a href="<?php echo esc_url(get_the_permalink($get_posts[0]->ID ));?>">
                            <img class="img-fluid" src="<?php echo esc_url($url_image_large);?>"  alt="<?php echo esc_attr($title_large);?>">
                        </a>
                        
                    </div>
                    <div class="content-blog-infor">
                        <h2>
                            <?php echo esc_html($title_large);?>
                        </h2>
                        <p class="short-description">
                            <?php echo htmlspecialchars_decode($excerpt_title);?>
                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-12">
                    <div class="row">
                        <?php foreach ($get_posts as $key => $post_new) {
                            if($key != 0){
                                $url_image_new = wp_get_attachment_url( get_post_thumbnail_id($post_new->ID,'full') );
                                $title_new = $post_new->post_title;
                            ?>
                                <div class="col-sm-6 col-12 item-blog-small">
                                    <div class="img-blog img-blog-large">
                                        <a href="<?php echo esc_url(get_the_permalink($post_new->ID ));?>">
                                            <img class="img-fluid" src="<?php echo esc_url($url_image_new);?>"  alt="<?php echo esc_attr($title_new);?>">
                                        </a>
                                        
                                    </div>
                                    <div class="content-blog-infor style-small">
                                        <h3>
                                            <?php echo esc_html($title_new);?>
                                        </h3>
                                    </div>
                                </div>
                            <?php }
                        }?>
                    </div>
                    
                </div>
            </div>

        <?php } ?>
    </section>
    
 <?php  
}
function new_post_relative_other($id_post, $id_term){
    $get_posts = get_posts( array( 'post_type'=> 'post', 
    'posts_per_page' =>5, 
    'post_status' => 'publish', 
    'post__not_in' => array($id_post),
    'orderby'=> 'post__not_in',
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'term_id',
            'terms'    => array($id_term),
        ),
    ),
    ) );
    if($get_posts){ ?>
    <ul>
        <?php
            foreach ($get_posts as $key => $recent) { 
                $words = 8;
                $more = ' …   ';
                
                $excerpt_title = wp_trim_words( get_the_title($recent->ID), $words, $more );
                ?>
                <li>
                    <a href="<?php echo get_the_permalink($recent->ID);?>"><i class='fas fa-caret-right'></i><?php echo $excerpt_title;?> <span> <?php echo  get_the_date('d/m/Y', $recent->ID);?></span></a>
                </li>
            <?php }

        ?>
       
       
    </ul>
<?php    }
}

if ( ! function_exists( 'tth_esc_data' ) ) {
	function tth_esc_data( $var_esc, $type = '' ) {
		switch ( $type ) {
			case 'html':
				return esc_html( $var_esc );
				break;
			case 'attr':
				return esc_attr( $var_esc );
				break;
			case 'url':
				return esc_url( $var_esc );
				break;
			case 'tags':
				$balance = 'balance';
				$tags    = 'Tags';
				$string  = call_user_func_array( $balance . $tags, [ $var_esc ] );

				return $string;
				break;
			case '':
			default:
				return $var_esc;
				break;
		}
	}
}
function tth_content_excerpt($limit){
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'.';
    } else {
        $excerpt = implode(" ",$excerpt);
    } 
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}
function tth_title_excerpt($limit){
    $excerpt = explode(' ', get_the_title(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'.';
    } else {
        $excerpt = implode(" ",$excerpt);
    } 
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}
function tth_pagination() {
    if( is_singular() )
        return;
    global $wp_query;
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 4 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<nav aria-label="Pagination Archive" class="ccv_pagination row"><ul class="col-12 text-center">' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li class="d-inline-block font-28">%s</li>' . "\n", get_previous_posts_link("<i class='fas fa-caret-left font-28'></i>") );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="d-inline-block active"' : 'class="d-inline-block"';
 
        printf( '<li %s class="d-inline-block"><a class="" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 4, $links ) )
            echo '<li class="d-inline-block"><a class="">…</a></li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="d-inline-block active"' : ' class="d-inline-block"';
        printf( '<li %s><a class="" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 2, $links ) )
            echo '<li class="d-inline-block"><a class="" >…</a></li>' . "\n";
 
        $class = $paged == $max ? ' class="d-inline-block active"' : ' class="d-inline-block"';
        printf( '<li %s><a class="" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li class="d-inline-block font-28">%s</li>' . "\n", get_next_posts_link("<i class='fas fa-caret-right font-28'></i>") );
 
    echo '</ul></nav>' . "\n";
}
//Send mail
add_action( 'wp_ajax_tth_sent_mail_contact', 'tth_sent_mail_contact' );
add_action( 'wp_ajax_nopriv_tth_sent_mail_contact', 'tth_sent_mail_contact' );
function tth_sent_mail_contact(){
    $fullname = isset($_GET['fullname']) ? trim($_GET['fullname']) : '';
    $subtitle = isset($_GET['subtitle']) ? trim($_GET['subtitle']) : '';
    $mail = isset($_GET['mail']) ? $_GET['mail'] : '';
    $content = isset($_GET['content']) ? $_GET['content'] : '';
    $phone = isset($_GET['phone']) ? $_GET['phone'] : '';
    $address = isset($_GET['address']) ? $_GET['address'] : '';
    $recaptcha = isset($_GET['recaptcha']) ? $_GET['recaptcha'] : '';
    $status = 0;
    $message ='';
    $subject_tth = $fullname;
    if(empty($recaptcha)){
        echo json_encode(
        array(
            'status' => 0,
            'message' => esc_html__('Captcha is required','matara').'<br>',
            )
        );
        die();
    }
    if(empty($fullname) || empty($subtitle) || (empty($mail))  || (empty($content)) || (empty($phone)) || (empty($address))){
        $status = 0;
        if(empty($fullname)){
            $message .= esc_html__('Vui lòng nhập họ tên','matara').'<br>';
        }
        if(empty($subtitle)){
            $message .= esc_html__('Vui lòng nhập tiêu đề','matara').'<br>';
        }
        if(empty($mail)){
            $message .= esc_html__('Vui lòng nhập  mail','matara').'<br>';
        }
        if(empty($phone)){
            $message .= esc_html__('Vui lòng nhập số điện thoại','matara').'<br>';
        }
        if(empty($content)){
            $message .= esc_html__('Vui lòng nhập nội dung','matara').'<br>';
        }
        if(empty($address)){
            $message .= esc_html__('Vui lòng nhập địa chỉ','matara').'<br>';
        }
        echo json_encode(
            array(
                'status' => $status,
                'message' => $message,
                )
            );
    } else {
        $status = 1;
        $theme_option = get_option('theme_option');
        $admin_email = isset($theme_option['email_admin_address']) ? $theme_option['email_admin_address'] : "";
        $body_email = "<html><body><h2>".$subject_tth."</h2>";
                        "<strong>".__('Họ và tên','matara')."</strong>: ".$subject_tth."<br/>".
                        "<strong>".__('Email','matara')."</strong>: ".$mail."<br/>".
                        "<strong>".__('Số điện thoại','matara')."</strong>: ".$phone."<br/>".
                        "<strong>".__('Nội dung','matara')."</strong>: ".$content."<br/></html></body>";

        $multiple_to_recipients = array($admin_email);
        $subject = $subject_tth;
        $body    = $body_email;
        $headers = 'From:' . $subject . "<" . $mail . ">" . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $headers .= 'Cc: '.$admin_email;
        $attachment = false;
        
        try {
            add_filter('wp_mail_content_type', 'set_html_content_type_sent_email');
            $check = tth_send_mail($multiple_to_recipients, $subject, $body, $headers,$attachment);
            tth_rm_filter('wp_mail_content_type', 'set_html_content_type_sent_email');
            if( $check){
                echo json_encode(
                    array(
                        'status' => $status,
                        'message' => esc_html__('Cám ơn bạn đã liên hệ với chúng tôi!<br>Chúng tôi sẽ liên hệ lại với bạn sớm nhất có thể.', 'matara'),
                        )
                );
            } else {
                echo json_encode(
                    array(
                        'status' => 0,
                        'message' => esc_html__('Lỗi gửi mail.', 'matara'),
                        )
                );
            }
        } catch (Exception $e) {
            echo json_encode(
                array(
                    'status' => 0,
                    'message' =>  $e->getMessage(),
                    )
            );
        }
        
        
        
    }
    die();
}
function set_html_content_type_sent_email()
{
    return 'text/html';
}
if(!function_exists('tth_send_mail')){
	function tth_send_mail($multiple_to_recipients, $subject, $body, $headers,$attachment){
		$check = wp_mail($multiple_to_recipients, $subject, $body, $headers,$attachment);
		return $check;
	}
}
if (!function_exists('tth_rm_filter')) {
	function tth_rm_filter($tag, $function_to_remove, $priority = 10)
	{
		return remove_filter( $tag, $function_to_remove, $priority );
	}
}
function mtr_box_contact(){
    $theme_option = get_option('theme_option');
    $mtr_contact_product = isset($theme_option['mtr_contact_product']) ? $theme_option['mtr_contact_product'] : "";
    $mtr_contact_text_product = isset($theme_option['mtr_contact_text_product']) ? $theme_option['mtr_contact_text_product'] : "";
    $mobile = str_replace(" ", "", $mtr_contact_product); ?>
    <div class="sidebar-ccv">
        <div class="item-sidebar">
            <div class="title-sb">
                <h3>
                    LIÊN HỆ VỚI MATARA
                </h3>
                
            </div>
        </div>
        <div class="content-sidebar">
            <ul>
                <li class="phone-contact">
                    <a href="tel:<?php echo esc_attr($mobile);?>"><span><i class="fas fa-phone-alt"></i></span><?php echo esc_html($mtr_contact_product);?></a>
                </li>
                <li class="active inline-text">
                    <em><?php echo esc_html($mtr_contact_text_product);?></em>
                </li>
            </ul>
        </div>
    </div>
<?php }
// function bbit_che_do_bao_tri()
// {
//     if (!current_user_can('edit_themes') || !is_user_logged_in()) {
//         wp_die('Trang web tạm thời đang được bảo trì. Xin vui lòng quay trở lại sau.');
//     }
// }
// add_action('get_header', 'bbit_che_do_bao_tri');

function ccv_form_dang_ky_tu_van(){ 
    ?>
    <div class="form_dangkytuvan col-12">
        <script type="text/javascript">
            var submitted = false;
        </script>
        <iframe class="lazy lazy-hidden" name="hidden_iframe" id="hidden_iframe" style="display: none;" onload="if(submitted) {window.location='vi/thank-you';}"></iframe>
        <noscript><iframe name="hidden_iframe" id="hidden_iframe" style="display: none;" onload="if(submitted) {window.location='vi/thank-you';}"></iframe></noscript>
        <form class="row"
            target="hidden_iframe"
            onsubmit="myButton.disabled = true;javascript: submitted=true;$('#formDangky').modal('hide');"
            action="https://docs.google.com/forms/u/0/d/e/1FAIpQLSfEXkwqtCgtFMyyb2984RZVEjpztqowFswJfxCWBN_nOsxKUQ/formResponse"
            method="post"
            id="mc-form">
            <div class="col-md-6 col-xs-12 col1">
                <div class="col-md-12 col-xs-12" style="margin-bottom: 15px;">
                    <input class="form-control" name="entry.1890291175" required="" id="entry_1963312297" dir="auto" placeholder="Họ &amp; tên học viên*" type="text" onkeyup="this.value = this.value.toUpperCase();" />
                    <div class="error-message" id="i.err.1537888300"></div>
                    <div class="ss-form-question errorbox-good" role="listitem"></div>
                </div>
                <div class="col-md-12 col-xs-12" style="margin-bottom: 15px;">
                    <div class="btn-group bootstrap-select form-control required">
                        <button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" data-id="ddlFruits1" title="2016">
                            <span class="filter-option pull-left">2016</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span>
                        </button>
                        <div class="dropdown-menu open" role="combobox">
                            <ul class="dropdown-menu inner" role="listbox" aria-expanded="false">
                                <li data-original-index="0" class="disabled selected">
                                    <a tabindex="-1" class="" data-tokens="null" role="option" href="#" aria-disabled="true" aria-selected="true"><span class="text">Năm sinh*</span><span class="glyphicon glyphicon-ok check-mark"></span></a>
                                </li>
                                <li data-original-index="1">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2017</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="2">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2016</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="3">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2015</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="4">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2014</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="5">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2013</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="6">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2012</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="7">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2011</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="8">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2010</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="9">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2009</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="10">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2008</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="11">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2007</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="12">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2006</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="13">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2005</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="14">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2004</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="15">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2003</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="16">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2002</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="17">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2001</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="18">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">2000</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="19">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1999</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="20">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1998</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="21">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1997</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="22">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1996</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="23">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1995</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="24">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1994</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="25">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1993</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="26">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1992</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="27">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1991</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="28">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1990</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="29">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1989</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="30">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1988</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="31">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1987</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="32">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1986</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="33">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1985</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="34">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1984</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="35">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1983</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="36">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1982</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="37">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1981</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="38">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1980</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="39">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1979</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="40">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1978</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="41">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1977</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="42">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1976</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="43">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1975</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="44">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1974</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="45">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1973</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="46">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1972</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="47">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1971</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="48">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1970</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="49">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1969</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="50">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1968</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="51">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1967</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="52">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1966</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="53">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1965</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="54">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1964</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="55">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1963</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="56">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1962</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="57">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1961</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="58">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">1960</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <select id="ddlFruits1" name="entry.1193648794" class="selectpicker form-control required" required="" placeholder="Năm sinh*" style="text-indent: 0;" tabindex="-98">
                            <option value="" selected="" disabled="">Năm sinh*</option>
                            <option value="2017" style="background-color: #fff; color: #000;">2017</option>
                            <option value="2016" style="background-color: #fff; color: #000;">2016</option>
                            <option value="2015" style="background-color: #fff; color: #000;">2015</option>
                            <option value="2014" style="background-color: #fff; color: #000;">2014</option>
                            <option value="2013" style="background-color: #fff; color: #000;">2013</option>
                            <option value="2012" style="background-color: #fff; color: #000;">2012</option>
                            <option value="2011" style="background-color: #fff; color: #000;">2011</option>
                            <option value="2010" style="background-color: #fff; color: #000;">2010</option>
                            <option value="2009" style="background-color: #fff; color: #000;">2009</option>
                            <option value="2008" style="background-color: #fff; color: #000;">2008</option>
                            <option value="2007" style="background-color: #fff; color: #000;">2007</option>
                            <option value="2006" style="background-color: #fff; color: #000;">2006</option>
                            <option value="2005" style="background-color: #fff; color: #000;">2005</option>
                            <option value="2004" style="background-color: #fff; color: #000;">2004</option>
                            <option value="2003" style="background-color: #fff; color: #000;">2003</option>
                            <option value="2002" style="background-color: #fff; color: #000;">2002</option>
                            <option value="2001" style="background-color: #fff; color: #000;">2001</option>
                            <option value="2000" style="background-color: #fff; color: #000;">2000</option>
                            <option value="1999" style="background-color: #fff; color: #000;">1999</option>
                            <option value="1998" style="background-color: #fff; color: #000;">1998</option>
                            <option value="1997" style="background-color: #fff; color: #000;">1997</option>
                            <option value="1996" style="background-color: #fff; color: #000;">1996</option>
                            <option value="1995" style="background-color: #fff; color: #000;">1995</option>
                            <option value="1994" style="background-color: #fff; color: #000;">1994</option>
                            <option value="1993" style="background-color: #fff; color: #000;">1993</option>
                            <option value="1992" style="background-color: #fff; color: #000;">1992</option>
                            <option value="1991" style="background-color: #fff; color: #000;">1991</option>
                            <option value="1990" style="background-color: #fff; color: #000;">1990</option>
                            <option value="1989" style="background-color: #fff; color: #000;">1989</option>
                            <option value="1988" style="background-color: #fff; color: #000;">1988</option>
                            <option value="1987" style="background-color: #fff; color: #000;">1987</option>
                            <option value="1986" style="background-color: #fff; color: #000;">1986</option>
                            <option value="1985" style="background-color: #fff; color: #000;">1985</option>
                            <option value="1984" style="background-color: #fff; color: #000;">1984</option>
                            <option value="1983" style="background-color: #fff; color: #000;">1983</option>
                            <option value="1982" style="background-color: #fff; color: #000;">1982</option>
                            <option value="1981" style="background-color: #fff; color: #000;">1981</option>
                            <option value="1980" style="background-color: #fff; color: #000;">1980</option>
                            <option value="1979" style="background-color: #fff; color: #000;">1979</option>
                            <option value="1978" style="background-color: #fff; color: #000;">1978</option>
                            <option value="1977" style="background-color: #fff; color: #000;">1977</option>
                            <option value="1976" style="background-color: #fff; color: #000;">1976</option>
                            <option value="1975" style="background-color: #fff; color: #000;">1975</option>
                            <option value="1974" style="background-color: #fff; color: #000;">1974</option>
                            <option value="1973" style="background-color: #fff; color: #000;">1973</option>
                            <option value="1972" style="background-color: #fff; color: #000;">1972</option>
                            <option value="1971" style="background-color: #fff; color: #000;">1971</option>
                            <option value="1970" style="background-color: #fff; color: #000;">1970</option>
                            <option value="1969" style="background-color: #fff; color: #000;">1969</option>
                            <option value="1968" style="background-color: #fff; color: #000;">1968</option>
                            <option value="1967" style="background-color: #fff; color: #000;">1967</option>
                            <option value="1966" style="background-color: #fff; color: #000;">1966</option>
                            <option value="1965" style="background-color: #fff; color: #000;">1965</option>
                            <option value="1964" style="background-color: #fff; color: #000;">1964</option>
                            <option value="1963" style="background-color: #fff; color: #000;">1963</option>
                            <option value="1962" style="background-color: #fff; color: #000;">1962</option>
                            <option value="1961" style="background-color: #fff; color: #000;">1961</option>
                            <option value="1960" style="background-color: #fff; color: #000;">1960</option>
                        </select>
                    </div>
                    <div class="error-message" id="i.err.1537888300"></div>
                    <div class="ss-form-question errorbox-good" role="listitem"></div>
                </div>
                <div class="col-md-12 col-xs-12" style="margin-bottom: 15px;">
                    <input class="form-control" name="entry.562719901" required="" id="phone" dir="auto" pattern="[0-9]{1}[0-9]{9}" placeholder="Số điện thoại liên hệ*" type="text" />
                    <div class="error-message" id="i.err.950792068"></div>
                    <div class="ss-form-question errorbox-good" role="listitem"></div>
                </div>
                <div class="col-md-12 col-xs-12" style="margin-bottom: 15px;">
                    <input class="form-control" name="entry.25988325" required="" id="email" dir="auto" placeholder="Email" type="email" />
                    <div class="error-message" id="i.err.950792068"></div>
                    <div class="ss-form-question errorbox-good" role="listitem"></div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 col2">
                <div class="col-md-12 col-xs-12" style="margin-bottom: 15px;">
                    <div class="btn-group bootstrap-select form-control required">
                        <button type="button" class="btn dropdown-toggle bs-placeholder btn-default" data-toggle="dropdown" role="button" data-id="ddlFruits2" title="Lựa chọn chi nhánh*">
                            <span class="filter-option pull-left">Lựa chọn chi nhánh*</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span>
                        </button>
                        <div class="dropdown-menu open" role="combobox">
                            <ul class="dropdown-menu inner" role="listbox" aria-expanded="false">
                                <li data-original-index="0" class="disabled selected">
                                    <a tabindex="-1" class="" data-tokens="null" role="option" href="#" aria-disabled="true" aria-selected="true">
                                        <span class="text">Lựa chọn chi nhánh*</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="1">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Hà Nội</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="2">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Hạ Long</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="3">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Thanh Hóa</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="4">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Hưng Yên</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="5">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Hải Dương</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="6">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Bắc Ninh</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="7">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Đà Nẵng</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="8">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Hà Giang</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="9">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Phú Thọ</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <select onchange="change(this);" id="ddlFruits2" name="entry.1404364156" class="selectpicker form-control required" required="" placeholder="Lựa chọn chi nhánh*" style="text-indent: 0;" tabindex="-98">
                            <option value="" selected="" disabled="">Lựa chọn chi nhánh*</option>
                            <option value="Hà Nội" style="background-color: #fff;">Hà Nội</option>
                            <option value="Hạ Long" style="background-color: #fff;">Hạ Long</option>
                            <option value="Thanh Hóa" style="background-color: #fff;">Thanh Hóa</option>
                            <option value="Hưng Yên" style="background-color: #fff;">Hưng Yên</option>
                            <option value="Hải Dương" style="background-color: #fff;">Hải Dương</option>
                            <option value="Bắc Ninh" style="background-color: #fff;">Bắc Ninh</option>
                            <option value="Đà Nẵng" style="background-color: #fff;">Đà Nẵng</option>
                            <option value="Hà Giang" style="background-color: #fff;">Hà Giang</option>
                            <option value="Phú Thọ" style="background-color: #fff;">Phú Thọ</option>
                        </select>
                    </div>
                    <div class="error-message" id="i.err.950004197"></div>
                    <div class="ss-form-question errorbox-good" role="listitem"></div>
                </div>
                <div class="col-md-12 col-xs-12" style="margin-bottom: 15px;">
                    <div class="btn-group bootstrap-select form-control required">
                        <button type="button" class="btn dropdown-toggle bs-placeholder btn-default" data-toggle="dropdown" role="button" data-id="ddlFruits3" title="Khu vực sinh sống">
                            <span class="filter-option pull-left">Khu vực sinh sống</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span>
                        </button>
                        <div class="dropdown-menu open" role="combobox">
                            <ul class="dropdown-menu inner" role="listbox" aria-expanded="false">
                                <li data-original-index="0" class="disabled selected">
                                    <a tabindex="-1" class="" data-tokens="null" role="option" href="#" aria-disabled="true" aria-selected="true"><span class="text">Khu vực sinh sống</span><span class="glyphicon glyphicon-ok check-mark"></span></a>
                                </li>
                            </ul>
                        </div>
                        <select id="ddlFruits3" name="entry.764272189" class="selectpicker form-control required" required="" placeholder="Khu vực sinh sống" style="text-indent: 0;" tabindex="-98">
                            <option value="" selected="" disabled="">Khu vực sinh sống</option>
                        </select>
                    </div>
                    <div class="error-message" id="i.err.950004197"></div>
                    <div class="ss-form-question errorbox-good" role="listitem"></div>
                </div>
                <div class="col-md-12 col-xs-12" style="margin-bottom: 15px;">
                    <div class="btn-group bootstrap-select form-control required">
                        <button type="button" class="btn dropdown-toggle bs-placeholder btn-default" data-toggle="dropdown" role="button" data-id="ddlFruits4" title="Khóa học quan tâm">
                            <span class="filter-option pull-left">Khóa học quan tâm</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span>
                        </button>
                        <div class="dropdown-menu open" role="combobox">
                            <ul class="dropdown-menu inner" role="listbox" aria-expanded="false">
                                <li data-original-index="0" class="disabled selected">
                                    <a tabindex="-1" class="" data-tokens="null" role="option" href="#" aria-disabled="true" aria-selected="true"><span class="text">Khóa học quan tâm</span><span class="glyphicon glyphicon-ok check-mark"></span></a>
                                </li>
                                <li data-original-index="1">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Tiếng Anh Mẫu giáo</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="2">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Tiếng Anh Chuyên Tiểu học</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="3">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Tiếng Anh Chuyên THCS</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="4">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Tiếng Anh Dự bị Đại học Quốc tế</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="5">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Tiếng Anh Luyện thi IELTS</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                                <li data-original-index="6">
                                    <a tabindex="0" class="" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                        <span class="text">Tiếng Anh Giao tiếp Chuyên nghiệp</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <select id="ddlFruits4" name="entry.975129895" class="selectpicker form-control required" placeholder="Khóa học quan tâm" style="text-indent: 0;" tabindex="-98">
                            <option value="" selected="" disabled="">Khóa học quan tâm</option>
                            <option value="Tiếng Anh Mẫu giáo" style="background-color: #fff; color: #000;">Tiếng Anh Mẫu giáo</option>
                            <option value="Tiếng Anh Chuyên Tiểu học" style="background-color: #fff; color: #000;">Tiếng Anh Chuyên Tiểu học</option>
                            <option value="Tiếng Anh Chuyên THCS" style="background-color: #fff; color: #000;">Tiếng Anh Chuyên THCS</option>
                            <option value="Tiếng Anh Dự bị Đại học Quốc tế" style="background-color: #fff; color: #000;">Tiếng Anh Dự bị Đại học Quốc tế</option>
                            <option value="Tiếng Anh Luyện thi IELTS" style="background-color: #fff; color: #000;">Tiếng Anh Luyện thi IELTS</option>
                            <option value="Tiếng Anh Giao tiếp Chuyên nghiệp" style="background-color: #fff; color: #000;">Tiếng Anh Giao tiếp Chuyên nghiệp</option>
                        </select>
                    </div>
                    <div class="error-message" id="i.err.950004197"></div>
                    <div class="ss-form-question errorbox-good" role="listitem"></div>
                </div>
                <div class="col-md-12 col-xs-12" style="margin-bottom: 15px;"><p style="color: #231f20; padding: 12px 0px 15px 15px;">* Thông tin bắt buộc</p></div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="col-md-12 col-xs-12">
                    <center><button onclick="return Validate()" type="submit" class="btn btn-dangkytuvan" id="btnsubmit" name="myButton">Đăng ký</button></center>
                    <div class="error-message" id="988295858_errorMessage"></div>
                    <div class="ss-form-question errorbox-good" role="listitem"></div>
                </div>
            </div>
        </form>

    </div>

<?php
}
<?php
    get_header();
    $theme_option = get_option('theme_option');
    $mtr_posts_per_page = get_option('posts_per_page',true);
    ?>
    <section class="container">
        <div class="row flex-column-reverse flex-md-row margin-top-24">
            <div class="col-12">
                <h3 class="text-left title-archive text-uppercase color-primary"> Kết quả tìm kiếm</h3>
            </div>
            
        </div>
        <div class="row list-tiem margin-top-24">
        <div class="col-sm-8 col-12">
                <?php
                if ( have_posts() ) {
                    while ( have_posts() ) :
                        the_post();
                        echo mtr_load_template( 'blog/content', '', ['posts_per_page' => $mtr_posts_per_page]);
                    endwhile;
                    echo tth_pagination();
                } else { ?>
                    <h4 class="text-left text-uppercase color-primary"> Không tìm thấy kết quả nào</h4>
                <?php }
                wp_reset_postdata();
                ?>
                
            
            </div>
            <div class="col-sm-4 col-12">
                <div class="sidebar-ccv">
                    <?php dynamic_sidebar( 'blog-sidebar' ) ?>
                </div>
                
            </div>
            
        </div>
        <?php echo tth_pagination();?>
        
    </section>
<?php get_footer();
?>
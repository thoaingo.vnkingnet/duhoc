
<?php $options = get_option( 'theme_option' );?>
<footer>
    <div id="doitac">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h2>Đối tác của AM Education</h2>
                    <div class="container-slider-partner owl-carousel owl-theme owl-loaded">
                    <?php if(!empty($options['doitac_list'])){ ?>
                        <?php foreach($options['doitac_list'] as $doitac_list){ ?>
                            <div class="item-logo">
                                <a href="<?php echo esc_url($doitac_list['url']);?>" target="_blank">
                                    <center>
                                        <img src="<?php echo esc_url($doitac_list['image']);?>" data-lazy-type="image" class="img-responsive lazy-loaded">
                                    </center>
                                </a>
                            </div>
                        <?php }?>
                    <?php }?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <a href="<?php echo get_home_url('/')?>">
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/logo-llv-white.png" alt="Language Link" width="230" height="49" />
                    </a>
                    <ul class="social">
                        <li>
                            <a rel="nofollow" href="<?php echo esc_url($options['linkfacebook']);?>">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/icon-fb.png" alt="Facebook" width="34" height="34" />
                            </a>
                        </li>
                        <li>
                            <a rel="nofollow" href="<?php echo esc_url($options['linkyoutube']);?>">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/icon-youtube.png" alt="Youtube" width="34" height="34" />
                            </a>
                        </li>
                        <li>
                            <a rel="nofollow" href="<?php echo esc_url($options['linkinsta']);?>">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/icon-instagram.png" alt="Instagram" width="34" height="34" />
                            </a>
                        </li>
                        <li>
                            <a href="mailto:<?php echo esc_attr($options['linkmail']);?>">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/icon-mail.png" alt="Email" width="34" height="34" />
                            </a>
                        </li>
                    </ul>
                    <?php
                        $phone = str_replace(' ', '',$options['phone-number']);
                    ?>
                    <a href="tel:<?php echo esc_attr($phone);?>" class="hotline_footer"> <img src="<?php echo get_template_directory_uri();?>/assets/images/icon-phone.png" alt="Hotline" width="26" height="25" style="margin-right: 8px;" /><?php echo esc_attr($options['phone-number']);?> </a>
                </div>
                <div class="col-md-3 col-xs-12 quicklinks">
                    <?php dynamic_sidebar( 'footer-1' ) ?>
                </div>
                <div class="col-md-2 col-xs-12 quicklinks mid-col">
                    <?php dynamic_sidebar( 'footer-2' ) ?>
                </div>
                <div class="col-md-4 col-xs-12 quicklinks">
                    <?php dynamic_sidebar( 'footer-3' ) ?>
                </div>
               
            </div>
            <?php 
                $all_lacation = get_posts(array('post_type'=>'cv_location','post_status'=>'publish','orderby'=>'date','order'=>'ASC'));
            ?>
            <hr class="footer_hr1">
            <div class="row row2">
                <?php 
                    if($all_lacation[0]){ ?>
                        <div class="col-md-12 col-xs-12 ">
                            <h3><?php echo get_the_title($all_lacation[0]->ID);?></h3>
                            <hr class="footer_hr">
                        </div>
                        <div class="col-md-12 col-xs-12 no-padding">
                            <?php 
                            $contentElementor = "";
                            if (class_exists("\\Elementor\\Plugin")) {
                                $pluginElementor = \Elementor\Plugin::instance();
                                $contentElementor = $pluginElementor->frontend->get_builder_content($all_lacation[0]->ID);
                            }
                            echo wp_kses_post($contentElementor);
                            ?>
                        </div>
                    <?php }
                ?>
                
            </div>
            <div class="row">
            <?php 
                foreach($all_lacation as $key=>$post){
                    
                    if($key !=0){ 
                        setup_postdata($post);
                        ?>
                        <div class="col-md-3 col-xs-6">
                            <h3><?php the_title();?></h3>
                            <hr class="footer_hr">
                            <?php 
                            $contentElementor = "";
                            if (class_exists("\\Elementor\\Plugin")) {
                                $pluginElementor = \Elementor\Plugin::instance();
                                $contentElementor = $pluginElementor->frontend->get_builder_content(get_the_ID());
                            }
                            echo wp_kses_post($contentElementor);
                            ?>
                        </div>
                    <?php }
                }
                wp_reset_postdata();
            ?>
            </div>
        </div>
    </div>
    <div id="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-12"><?php echo esc_html($options['copyright']);?></div>
                <div class="col-md-9 col-xs-12">
                    <?php
                        $footermenu = array(
                            'theme_location'  => 'footer',
                            'menu'            => '',
                            'container'       => '',
                            'container_class' => '',
                            'container_id'    => '',
                            'menu_class'      => 'copyright',
                            'menu_id'         => '',
                            'echo'            => true,
                            'before'          => '',
                            'after'           => '',
                            'link_before'     => '',
                            'link_after'      => '',
                            'items_wrap'      => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
                            'depth'           => 0,
                            );
                            if ( has_nav_menu( 'footer' ) ) {
                            wp_nav_menu( $footermenu );
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
<div class="fix-sidebar">
    <ul class="fix-sidebar-1">
        <li>
            <a rel="nofollow" href="<?php echo (!empty($options['copyright']) ? $options['copyright'] : ''); ?>" target="_blank">
                <div><img src="<?php echo get_template_directory_uri()?>/assets/images/icon-fix-1.png" alt="" /></div>
                <div>Facebook</div>
            </a>
        </li>
        <!--<li><a rel="nofollow" href="https://zalo.me/4507656224209365490" target="_blank"><div><img src="<?php echo get_template_directory_uri()?>/assets/images/icon-fix-2.png" alt=""></div><div>Zalo</div> </a></li>-->
        <li class="llv-pc">
            <?php $phone_footer = str_replace(' ', '',$options['ccv_phone']); ?>
            <a href="tel:<?php echo esc_attr($phone_footer);?>">
                <div><img src="<?php echo get_template_directory_uri()?>/assets/images/icon-fix-3.png" alt="" /></div>
                <div><?php echo esc_html(!empty($options['ccv_phone']) ? $options['ccv_phone'] : ''); ?></div>
            </a>
        </li>
        <li class="llv-pc">
            <a href="<?php echo (!empty($options['ccv-download']) ? $options['ccv-download'] : '');  ?>/">
                <div><img src="<?php echo get_template_directory_uri()?>/assets/images/icon-fix-4.png" alt="" /></div>
                <div>
                    Tải xuống tài liệu <br />
                    miễn phí
                </div>
            </a>
        </li>
        <li class="llv-pc">
            <a href="<?php echo (!empty($options['ccv-gmail']) ? $options['ccv-gmail'] : '');  ?>/">
                <div><img src="<?php echo get_template_directory_uri()?>/assets/images/icon-fix-5.png" alt="" /></div>
                <div>
                    Kiểm tra trình độ <br />
                    tiếng Anh miễn phí
                </div>
            </a>
        </li>
        <li class="llv-mb">
            <a href="https://llv.edu.vn/download/">
                <div><img src="<?php echo get_template_directory_uri()?>/assets/images/icon-fix-4.png" alt="" /></div>
                <div>Tải tài liệu</div>
            </a>
        </li>
        <li class="llv-mb">
            <a href="https://llv.edu.vn/exams/">
                <div><img src="<?php echo get_template_directory_uri()?>/assets/images/icon-fix-5.png" alt="" /></div>
                <div>Kiểm tra</div>
            </a>
        </li>
    </ul>
    <div class="llv-pc fix-sidebar-2">
        <script type="text/javascript">
            var submitted = false;
        </script>
       <!-- <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdAVz7LNpjX5yAAvvgeijG78HOgGeZj3HUfbVrG5J_QVqoqJA/viewform?embedded=true" width="640" height="848" frameborder="0" marginheight="0" marginwidth="0">Đang tải…</iframe> -->
        <iframe
            name="hidden_iframe_fix"
            id="hidden_iframe_fix"
            style="display: none;"
            onload="if(submitted){alert('Cảm ơn bạn đã gửi thông tin đăng ký \n amedu sẽ liên lạc lại trong thời gian sớm nhất!');window.location='https://amedu.edu.vn/';}"
        ></iframe>
        
        <form target="hidden_iframe_fix" action="https://docs.google.com/forms/d/e/1FAIpQLSdAVz7LNpjX5yAAvvgeijG78HOgGeZj3HUfbVrG5J_QVqoqJA/formResponse" method="post" onsubmit="return checkphone()" id="form-fix">
            <div class="fix-sidebar-2-expand-btn">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/icon-fix-6.png" alt="" />
                <p>Đăng ký tư vấn ngay!</p>
            </div>
            <div class="fix-sidebar-2-form">
                <input type="text" name="entry.670626160" placeholder="Họ &amp; tên học viên" required="" /> <input type="number" name="entry.1160595059" placeholder="Năm sinh" required="" />
                <input type="tel" name="entry.858088358" id="txtphone-fix" placeholder="Số điện thoại" required="" />
                <select id="" name="entry.1317135789" class="wow pulse" title="Lựa chọn trung tâm tư vấn" required="">
                    <option value="" disabled="disabled" selected="selected">Lựa chọn trung tâm tư vấn</option>
                    <option value="Lựa chọn trung tâm tư vấn 1">Lựa chọn trung tâm tư vấn 1</option>
                    <option value="Lựa chọn trung tâm tư vấn 2">Lựa chọn trung tâm tư vấn 2</option>
                    <option value="Lựa chọn trung tâm tư vấn 3">Lựa chọn trung tâm tư vấn 3</option>
                </select>
            </div>
            <div class="fix-sidebar-2-submit">
                <button type="submit" id="btn-fix-register" class="btn-fix-register">Gửi</button>
                <div id="loading"><img src="<?php echo get_template_directory_uri()?>/assets/images/loading.gif" alt="" /></div>
            </div>
        </form>
    </div>
</div>
<?php wp_footer();?>





<!-- 
<?php
    $theme_option = get_option('theme_option');
    $matara_slider_partner = isset($theme_option['matara_slider_partner']) ? $theme_option['matara_slider_partner'] : array();
    $matara_partner_title = isset($theme_option['matara_partner_title']) ? $theme_option['matara_partner_title'] : "";
    $matara_partner_content = isset($theme_option['matara_partner_content']) ? $theme_option['matara_partner_content'] : "";
    $matara_link_facebook = isset($theme_option['matara_link_facebook']) ? $theme_option['matara_link_facebook'] : "";
    $matara_link_youtube = isset($theme_option['matara_link_youtube']) ? $theme_option['matara_link_youtube'] : "";
    $matara_link_twitter = isset($theme_option['matara_link_twitter']) ? $theme_option['matara_link_twitter'] : "";
    $matara_main_footer = isset($theme_option['matara_main_footer']) ? $theme_option['matara_main_footer'] : "";
    $matara_copyright = isset($theme_option['matara_copyright']) ? $theme_option['matara_copyright'] : "";
?>
            <section class="full-width back-bg-green partner margin-top-40">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h3 class="text-center color-primary">
                                <?php echo esc_html($matara_partner_title);?>
                            </h3>
                            <div class="content-about">
                                <?php echo htmlspecialchars_decode($matara_partner_content);?>
                            </div>
                        </div>
                    </div>
                    <?php 
                        if(!empty($matara_slider_partner)){ ?>
                            <div class="row">
                                <div class="owl-carousel owl-theme">
                                    <?php
                                        foreach ($matara_slider_partner as $key => $logo_partner) {
                                    ?>
                                    <div class="item">
                                        <img class="d-block img-fluid" src="<?php echo esc_url($logo_partner['image']);?>" alt="<?php echo esc_attr($logo_partner['title']);?>">
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                        <?php }
                    ?>
                </div>
            </section>
		</main>
		<footer>
			<div class="top-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-12 d-sm-flex align-items-end justify-content-between">
                            <ul class="social">
                                <li class="facebook">
                                    <a href="<?php echo esc_url($matara_link_facebook);?>"><i class="fab fa-facebook"></i></a>
                                </li>
                                <li class="youtube">
                                    <a href="<?php echo esc_url($matara_link_youtube);?>"><i class="fab fa-youtube"></i></a>
                                </li>
                                <li class="twitter">
                                    <a href="<?php echo esc_url($matara_link_twitter);?>"><i class="fab fa-twitter"></i></a>
                                </li>
                            </ul>
                            <div class="menu-footer main-menu">
                            <?php
									$primarymenu = array(
										'theme_location'  => 'footer',
										'menu'            => '',
										'container'       => '',
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => 'd-sm-flex justify-content-end',
										'menu_id'         => '',
										'echo'            => true,
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 0,
										'walker' => new Walker_Nav_Menu_Custom()
										);
										if ( has_nav_menu( 'footer' ) ) {
										wp_nav_menu( $primarymenu );
									}
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-footer text-center">
                <?php echo htmlspecialchars_decode($matara_main_footer);?>
            </div>
            <div class="bottom-footer">
                <p class="text-center"><?php echo esc_html($matara_copyright);?></p>
            </div>
        </footer>
        <?php wp_footer();?>
        <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0&appId=275241790088186&autoLogAppEvents=1"></script>
    </body>
</html> -->
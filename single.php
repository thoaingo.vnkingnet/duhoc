<?php 
get_header();
?>
<?php $theme_option = get_option('theme_option');
$banner_new = isset($theme_option['banner_new']) ? $theme_option['banner_new']['url'] : "";

if(have_posts()): the_post();
$term = wp_get_post_terms(get_the_ID(),'category')[0];
$id_term = $term->term_id;
$name_term = $term->name;
$get_post_time = get_the_time('', get_the_ID());
$get_the_date = get_the_date('d/m/Y', get_the_ID());
$thumbnail = get_the_post_thumbnail_url( get_the_ID(),  $size = 'full' ); ?>


<section class="container single-post">
    <div class="row">
        <div class="col-sm-8 col-12">
            <img class="img-fluid" src="<?php echo esc_url($thumbnail);?>" alt="">
            <div class="row flex-column-reverse flex-md-row margin-top-24">
                <div class="col-12">
                    <h1 class="title text-left text-uppercase color-primary border-dots entry-title">
                        <?php the_title();?>
                    </h1>
                </div>
            </div>
            <div class="date-post">
                <em><?php echo $get_post_time;?>, <?php echo esc_html($get_the_date);?></em>
            </div>
            <div class="content-single-post">
                <?php the_content();?>
            </div>
            <?php 
                $related = get_posts( array( 'category__in' => wp_get_post_categories(get_the_ID()), 'numberposts' => 5, 'post__not_in' => array(get_the_ID()) ) );
                if( $related ){
                ?>
                    <div class="product-relation">
                        <h3>
                            TIN TỨC KHÁC
                        </h3>
                        <?php echo new_post_relative_other(get_the_ID(),$id_term);?>
                    </div>
                <?php }?>
        </div>
        <div class="col-sm-4 col-12">
            <div class="sidebar-ccv">
                <?php dynamic_sidebar( 'blog-sidebar' ) ?>
            </div>
            
        </div>
    </div>
</section>
<?php
endif;
get_footer();?>
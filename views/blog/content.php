<?php

$class = "item-post ";
if ( isset( $cols ) ) {
	if ( $cols == 3 ) {
		$class = "item-post";
	}
}
$post_categories = wp_get_post_categories( get_the_ID() );
?>

<div class="row single-item">
    <div class="col-md-4 col-xs-5">
        <a href="<?php the_permalink();?>/">
            <?php if ( has_post_thumbnail() ) {
            $url_image_large = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID(),'full') );
            ?>
            <div class="image">
                <a href="<?php the_permalink();?>">
                    <img class="img-fluid" src="<?php echo esc_url($url_image_large);?>" alt="<?php the_title();?>">
                </a>
            </div>
            <?php }?>
        </a>
    </div>
    <div class="col-md-8 col-xs-7">
        <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
        <p><span class="created-date"><?php echo get_the_date('d.m.Y', get_the_ID());?></span></p>
        <div class="intro-text">
            <p>
                <?php echo tth_content_excerpt(50)?>
            </p>
        </div>
    </div>
</div>



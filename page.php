<?php 
get_header();
if(have_posts()) : the_post();
$url_image_large = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID(),'full') );
?>
<section class="banner  main-section" style="background: url('<?php echo esc_url($url_image_large);?>') no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="section-part text-left">
                <h3 class="text-left text-white font-size-30">
                    <?php the_title();?>
                </h3>
            </div>
        </div>
    </div>
    <div id="overlay"></div>
</section>
<section class="content-page container full-width margin-top-40">
    <?php the_content();?>
</section>
<?php
endif;
get_footer();?>
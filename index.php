<?php get_header();
?>
<?php 
$options = get_option( 'theme_option' );
?>
    <div id="wraper">
        <section class="ccv-banner" style="background: url('<?php echo esc_url($options['image-banner']);?>') center center no-repeat;">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="content-banner-wrapper">
                            <div class="content-banner">
                                <h1 class="revealOnScroll animated fadeInDown" data-animation="fadeInDown">
                                    <?php echo wp_kses_post( $options['title'] )?>
                                </h1>
                                <p class="revealOnScroll animated fadeInUp" data-animation="fadeInUp">
                                    <?php echo wp_kses_post( $options['subtitle'] )?>
                                </p>
                                <div class="conso">
                                    <img src="<?php echo esc_url($options['image-nam-toancau']);?>" data-lazy-type="image"
                                        data-src="<?php echo esc_url($options['image-nam-toancau']);?>" width="47" height="60"
                                        class="revealOnScroll zoomIn lazy-loaded animated" data-animation="zoomIn">
                                   
                                    <img src="<?php echo esc_url($options['image-nam-taivietnam']);?>" data-lazy-type="image"
                                        data-src="<?php echo esc_url($options['image-nam-taivietnam']);?>" width="47" height="60"
                                        class="revealOnScroll zoomIn lazy-loaded animated" data-animation="zoomIn">
                                </div>
                                <div class="button-herobanner">
                                    <?php
                                        if(!empty($options['button-banner'])){
                                            foreach($options['button-banner'] as $item_button){ ?>
                                                <a href="<?php echo esc_url($item_button['link'] ? $item_button['link'] : "#");?>" class="discover revealOnScroll fadeIn animated" data-animation="fadeIn"><?php echo esc_html($item_button['title'] ? $item_button['title'] : '');?></a>
                                            <?php }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        <?php if(!empty($options['co-gi-dang-dien-ra'])){ ?>
            <section id="slider_link_video">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="revealOnScroll fadeIn animated" data-animation="fadeIn">Có gì đang diễn ra?</h2>
                        </div>
                        <div class="col-xs-12 col-md-12">
                            <div class="swiper-container swiper-container-slider-video">
                                <div class="swiper-wrapper">
                                    <!-- Slides -->
                                    <?php 
                                        
                                    foreach($options['co-gi-dang-dien-ra'] as $item_slider_cg){ ?>
                                        <div class="swiper-slide">
                                            <a href="<?php echo esc_url($item_slider_cg['url'] ? $item_slider_cg['url'] : '#' );?>" target="_blank">
                                                <img src="<?php echo esc_url($item_slider_cg['image'] ? $item_slider_cg['image'] : '#' );?>" class="slider-9946 slide-25711" alt="" sizes="(max-width: 1200px) 100vw, 1200px" draggable="false">
                                            </a>
                                        </div>
                                    <?php }
                                    ?>
                                    
                                    <div class="swiper-slide">
                                        <a href="#" target="_blank">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/images/Video3.jpg" class="slider-9946 slide-25711" alt="" sizes="(max-width: 1200px) 100vw, 1200px" draggable="false">
                                        </a>
                                    </div>
                                    <div class="swiper-slide">
                                        <a href="#" target="_blank">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/images/Video4.jpg" class="slider-9946 slide-25711" alt="" sizes="(max-width: 1200px) 100vw, 1200px" draggable="false">
                                        </a>
                                    </div>
                                    <div class="swiper-slide">
                                        <a href="#" target="_blank">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/images/Video5.jpg" class="slider-9946 slide-25711" alt="" sizes="(max-width: 1200px) 100vw, 1200px" draggable="false">
                                        </a>
                                    </div>
                                    <div class="swiper-slide">
                                        <a href="#" target="_blank">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/images/Video6.jpg" class="slider-9946 slide-25711" alt="" sizes="(max-width: 1200px) 100vw, 1200px" draggable="false">
                                        </a>
                                    </div>
                                </div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="ccv-swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </section>
        <?php }?>
        <?php if(!empty($options['courses_home'])){ ?>
        <div id="courses_home" style="background: url('<?php echo get_template_directory_uri()?>/assets/images/bg-courses.jpg') #fff center top repeat;">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12">
                        <h2>Học ở đây. <br><span style="font-weight:300;">Graduate anywhere.</span></h2>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row row_courses">
                    <?php
                        foreach($options['courses_home'] as $courses_home){ ?>
                            <div class="col-6 col-md-2 col-md-offset-1 courses">
                                <div class="courses-height">
                                    <h3>
                                        <a href="<?php echo esc_url( $courses_home['url'] );?>">
                                            <?php echo wp_kses_post( $courses_home['name_course'] );?>
                                        </a>
                                    </h3>
                                    <p><a href="#">Xem thêm <img class="lazy-loaded" src="<?php echo get_template_directory_uri()?>/assets/images/arrow-courses.png" style="margin:1px 0px 0px 3px;"></a></p>
                                </div>
                                <div class="image">
                                    <a href="<?php echo esc_url( $courses_home['url'] );?>">
                                        <img src="<?php echo esc_url( $courses_home['image'] );?>" data-lazy-type="image" class="img-fluid lazy-loaded">
                                    </a>
                                </div>
                            </div>
                        <?php }
                    ?>
                    
                </div>
            </div>
        </div>
        <?php }?>
        
        <div id="usp_home">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <?php if(!empty($options['usp_home_title'])){?>
                            <?php echo wp_kses_post( $options['usp_home_title'] )?>
                        <?php }?>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php if(!empty($options['usp_image-banner'])){?>
                            <img src="<?php echo esc_url($options['usp_image-banner']);?>" class="img-fluid hidden-xs lazy-loaded">
                        <?php }?>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-4">
                    <?php if(!empty($options['usp_image'])){?>
                        <center>
                            <img src="<?php echo esc_url($options['usp_image']);?>"  class="img-fluid lazy-loaded">
                        </center>
                    <?php }?>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="white-box">
                            <?php if(!empty($options['usp_content'])){?>
                                <?php echo wp_kses_post( $options['usp_content'] )?>
                            <?php }?>
                            
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                    <?php if(!empty($options['usp_box-right'])){
                        echo wp_kses_post( $options['usp_box-right'] );
                    }?>
                    </div>
                </div>
                <div class="row">
                    <?php if(!empty($options['usp_box_list'])){
                        foreach($options['usp_box_list'] as $usp_box_list){ ?>
                        <div class="col-md-4 col-12">
                            <div class="green-box">
                                <p class="highlight_number twoline">
                                    <span class="counter" data-count="<?php echo esc_attr($usp_box_list['number']);?>"><?php echo esc_html($usp_box_list['number']);?></span>
                                </p>
                                <?php echo wp_kses_post( $usp_box_list['text'] );?>
                            </div>
                        </div>
                    <?php }
                    }?>
                </div>
            </div>
        </div>
    </div>

    <div id="tuvan">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="heading">Bạn quan tâm?</h3>
                    <p style="font-size:16px; color:#231f20; text-align:center; padding-left:20px; padding-right:20px;">
                        Vui lòng để lại thông tin bên dưới để nhận tư vấn về các chương trình, khóa học, và tham dự lớp học mẫu miễn phí!
                    </p>
                </div>
            </div>
            <div class="row">
                <?php echo ccv_form_dang_ky_tu_van();?>
            </div>
        </div>
    </div>


    <div id="khamphadiadiem">
        <div class="khamphadiadiem-map-left" style="background: url(<?php echo get_template_directory_uri()?>/assets/images/llf-map1.png) left 15% bottom no-repeat;">
            <div class="khamphadiadiem-map-right" style="background: url(<?php echo get_template_directory_uri()?>/assets/images/llf-map2.png) right 15% top no-repeat;">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-12">
                            <h2>AM Education <span class="line2">đã có mặt tại nhiều tỉnh thành trên cả nước!</span></h2>
                        </div>
                    </div>
                    <div class="row jumplink">
                        <div class="col-md-2 col-12"></div>
                        <div class="col-md-6 col-12 col1">
                            <div class="btn-group bootstrap-select form-control required">
                                <button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" data-id="branch" title="Hưng Yên">
                                    <span class="filter-option pull-left">Hưng Yên</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span>
                                </button>
                                <div class="dropdown-menu open" role="combobox">
                                    <ul class="dropdown-menu inner" role="listbox" aria-expanded="false">
                                        <li data-original-index="0" class="disabled selected">
                                            <a tabindex="-1" class="" data-tokens="null" role="option" href="#" aria-disabled="true" aria-selected="true"><span class="text">Lựa chọn chi nhánh*</span><span class="glyphicon glyphicon-ok check-mark"></span></a>
                                        </li>
                                        <li data-original-index="1">
                                            <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                                <span class="text">Hạ Long</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                            </a>
                                        </li>
                                        <li data-original-index="2">
                                            <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                                <span class="text">Thanh Hóa</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                            </a>
                                        </li>
                                        <li data-original-index="3">
                                            <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                                <span class="text">Hưng Yên</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                            </a>
                                        </li>
                                        <li data-original-index="4">
                                            <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                                <span class="text">Hải Dương</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                            </a>
                                        </li>
                                        <li data-original-index="5">
                                            <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                                <span class="text">Đà Nẵng</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                            </a>
                                        </li>
                                        <li data-original-index="6">
                                            <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                                <span class="text">Bắc Ninh</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                            </a>
                                        </li>
                                        <li data-original-index="7">
                                            <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                                <span class="text">Hà Giang</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                            </a>
                                        </li>
                                        <li data-original-index="8">
                                            <a tabindex="0" class="" style="background-color: rgb(255, 255, 255);" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">
                                                <span class="text">Phú Thọ</span><span class="glyphicon glyphicon-ok check-mark"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <select id="branch" name="" class="selectpicker form-control required" required="" placeholder="Lựa chọn chi nhánh*" style="text-indent: 0; padding-left: 10px; color: #333;" tabindex="-98">
                                    <option value="" selected="" disabled="">Lựa chọn chi nhánh*</option>
                                    <option value="<?php echo get_home_url('/');?>" style="background-color: #fff;">Hạ Long</option>
                                    <option value="<?php echo get_home_url('/');?>" style="background-color: #fff;">Thanh Hóa</option>
                                    <option value="<?php echo get_home_url('/');?>" style="background-color: #fff;">Hưng Yên</option>
                                    <option value="<?php echo get_home_url('/');?>" style="background-color: #fff;">Hải Dương</option>
                                    <option value="<?php echo get_home_url('/');?>" style="background-color: #fff;">Đà Nẵng</option>
                                    <option value="<?php echo get_home_url('/');?>" style="background-color: #fff;">Bắc Ninh</option>
                                    <option value="<?php echo get_home_url('/');?>" style="background-color: #fff;">Hà Giang</option>
                                    <option value="<?php echo get_home_url('/');?>" style="background-color: #fff;">Phú Thọ</option>
                                </select>
                            </div>    
                        </div>
                        <div class="col-md-2 col-12 col2">
                            <center><button type="submit" class="btn btn-dangkytuvan" id="goto" name="myButton">Đi tới</button></center>
                        </div>              
                    </div>
                    <p class="note">Lựa chọn địa điểm bạn muốn ghé thăm, ở lại trang này nếu bạn đến từ Hà Nội.</p>
                </div>
            </div>
        </div>
    </div>
    <div id="pr">
        <div class="container">
            <div class="row">
                <!--COL1-->
                <div class="col1 col-12 col-md-6">
                    <h2>
                        <center>Từ AM Education
                            <br>
                            <span style="font-weight:400"> tới thế giới</span>
                        </center>
                    </h2>
                    <?php $category_one = $options['category_one'] ? $options['category_one'] : '';
                        $post_item_1 = get_posts(array(
                            'post_type' => 'post',
                            'showposts' => 3,
                            'tax_query' => array(
                              array(
                                'taxonomy' => 'category',
                                'field' => 'term_id', 
                                'terms' => $category_one,
                              )
                            )
                          ));
                    if(!empty($post_item_1)){ 
                        ?>
                        <div class="row highlight">
                            <div class="col-12 col-md-6">
                                <center>
                                    <?php echo get_the_post_thumbnail($post_item_1[0]->ID,['500','262']);?>
                                </center>
                            </div>
                            <div class="col-12 col-md-6">
                                <a class="pr-title" href="<?php echo get_the_permalink( $post_item_1[0]->ID )?>"><?php echo get_the_title($post_item_1[0]->ID);?></a>
                                <p class="intro-text"><?php echo get_the_excerpt($post_item_1[0]->ID);?></p>
                            </div>
                        </div>
                   
                        <div class="row related">
                            <ul class="list-related">
                                <?php 
                                    foreach($post_item_1 as $key=>$item_1){
                                        if($key != 0){ ?>
                                            <li style="background: url(<?php echo get_template_directory_uri()?>/assets/images/bullet.png) 0 8px no-repeat;">
                                            <a href="<?php echo get_the_permalink($item_1->ID)?>"><?php echo esc_html($item_1->post_title);?></a>
                                        </li>
                                        <?php }
                                    }
									wp_reset_postdata();
                                ?>
                            </ul>
                        </div>
                        <p>
                            <a href="<?php echo get_category_link($category_one);?>" target="_blank" class="view-all">Xem tất cả</a>
                        </p>
                    <?php }
                        
                    ?>
                    
                </div>
                <!--END COL1-->
                <!--COL2-->
                <div class="col2 col-12 col-md-6">
                    <h2>
                        <center>Từ thế giới
                            <br>
                            <span style="font-weight:400"> tới AM Education</span>
                        </center>
                    </h2>
                    <?php $category_two = $options['category_two'] ? $options['category_two'] : '';
                        $post_item_2 = get_posts(array(
                            'post_type' => 'post',
                            'showposts' => 3,
                            'tax_query' => array(
                              array(
                                'taxonomy' => 'category',
                                'field' => 'term_id', 
                                'terms' => $category_two,
                              )
                            )
                          ));
                        if(!empty($post_item_2)){ 
                            ?>
                            <div class="row highlight">
                                <div class="col-12 col-md-6">
                                    <center>
                                        <?php echo get_the_post_thumbnail($post_item_2[0]->ID,['500','262']);?>
                                    </center>
                                </div>
                                <div class="col-12 col-md-6">
                                    <a class="pr-title" href="<?php echo get_the_permalink( $post_item_2[0]->ID )?>"><?php echo get_the_title($post_item_2[0]->ID);?></a>
                                    <p class="intro-text"><?php echo get_the_excerpt($post_item_2[0]->ID);?></p>
                                </div>
                            </div>
                    
                            <div class="row related">
                                <ul class="list-related">
                                    <?php 
                                        foreach($post_item_2 as $key=>$item_2){
                                            if($key != 0){ ?>
                                                <li style="background: url(<?php echo get_template_directory_uri()?>/assets/images/bullet.png) 0 8px no-repeat;">
                                                <a href="<?php echo get_the_permalink($item_2->ID)?>"><?php echo esc_html($item_2->post_title);?></a>
                                            </li>
                                            <?php }
                                        }
									wp_reset_postdata();
                                    ?>
                                </ul>
                            </div>
                            <p>
                                <a href="<?php echo get_category_link($category_two);?>" target="_blank" class="view-all">Xem tất cả</a>
                            </p>
                        <?php }
                    ?>
                </div>
                <!--END COL2-->
            </div>
        </div>
    </div>
    <div id="nhatky" style="background: url(<?php echo get_template_directory_uri()?>/assets/images/bg-nhatky.jpg) #ddd center bottom no-repeat;">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-6 col-12 heading" style="background: url(<?php echo get_template_directory_uri()?>/assets/images/icon-camera.png) #063 50px center no-repeat;">
                    <h2><?php echo wp_kses_post($options['nhatky_title']);?></h2>
                    <p><?php echo wp_kses_post($options['nhatky_content']);?></p>
                </div>
            </div>
            <?php if(!empty($options['nhatky_video'])){ ?>
            <div class="row nhatky">
                <div class="col-12">
                    <div class="container-slider-video owl-carousel owl-theme owl-loaded">
                    <?php foreach($options['nhatky_video'] as $nhatky_video){ 
                        parse_str( parse_url( $nhatky_video['url'], PHP_URL_QUERY ), $id_youtuve );
                        
                        ?>
                        <div class="item">
                            <div class="video">
                                <div class="youtube" data-embed="<?php echo esc_attr($id_youtuve['v']);?>">
                                    <div class="play-button"><i class="fab fa-youtube"></i></div>
                                </div>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    
                </div>
            </div>
            <?php }?>
        </div>
    </div>
    

<?php get_footer();?>

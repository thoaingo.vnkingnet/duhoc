<?php
class RecentNews extends \WP_Widget {
    function __construct() {
        parent::__construct(
            'recentnews', // Base ID
            esc_html__( 'Matara - Tin tức mới', 'matara' ), // Name
            array( 'description' => esc_html__( 'Tin tức mới', 'matara' ), ) // Args
        );
    }
    // Widget Backend - anything show in widget
    function form( $instance ) {
        global $wpdb;
        $instance = wp_parse_args( (array) $instance, array('title_recentnews' => '','numbershow'=>'','order_recent' =>'','order_by_recent' =>'' ) );
        $title_recentnews = htmlspecialchars($instance['title_recentnews']);
            $numbershow = htmlspecialchars($instance['numbershow']);
            $order_by_recent = htmlspecialchars($instance['order_by_recent']);
            $order_recent = htmlspecialchars($instance['order_recent']);
            
        ?>
         <p>
            <label for="<?php echo esc_attr($this->get_field_id('title_recentnews'))?>"><?php echo esc_html__( 'Title', 'matara' )?></label>
            <input type="text" name="<?php echo esc_attr($this->get_field_name('title_recentnews'))?>" id="<?php echo esc_attr($this->get_field_id('title_recentnews'));?>" style="width:100%" value="<?php echo esc_attr($title_recentnews ); ?>">
            
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('numbershow'))?>"><?php echo esc_html__( 'Number show', 'matara' )?></label>
            <input type="text" name="<?php echo esc_attr($this->get_field_name('numbershow'))?>" id="<?php echo esc_attr($this->get_field_id('numbershow'));?>" style="width:100%" value="<?php echo esc_attr($numbershow) ?>">
        </p>
        <p>
        <select id="<?php echo esc_attr($this->get_field_id( 'order_by_recent' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'order_by_recent' )); ?>" class="widefat" style="width:100%;">
        <option <?php if ( 'ID' == $instance['order_by_recent'] ) echo 'selected="selected"'; ?> value="ID"><?php echo esc_html__( 'ID', 'matara' )?></option>
        <option <?php if ( 'author' == $instance['order_by_recent'] ) echo 'selected="selected"'; ?> value="author"><?php echo esc_html__( 'Author', 'matara' )?></option>
        <option <?php if ( 'box3' == $instance['order_by_recent'] ) echo 'selected="selected"'; ?> value="box3"><?php echo esc_html__( 'Date', 'matara' )?></option>
         <option <?php if ( 'name' == $instance['order_by_recent'] ) echo 'selected="selected"'; ?> value="name"><?php echo esc_html__( 'Name', 'matara' )?></option>
        </select>
        </p>
        <p>
             <select id="<?php echo esc_attr($this->get_field_id( 'order_recent' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'order_recent' )); ?>" class="widefat" style="width:100%;">
            <option <?php if ( 'ASC' == $instance['order_recent'] ) echo 'selected="selected"'; ?> value="ASC"><?php echo esc_html__( 'ASC', 'matara' )?></option>
            <option <?php if ( 'DESC' == $instance['order_recent'] ) echo 'selected="selected"'; ?> value="DESC"><?php echo esc_html__( 'DESC', 'matara' )?></option>
           
            </select>
           
        </p>

        <?php
    }
    // Updating widget replacing old instances with new
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title_recentnews'] = stripcslashes($new_instance['title_recentnews']);
        $instance['numbershow'] = stripcslashes($new_instance['numbershow']);
        $instance['order_by_recent'] = stripcslashes($new_instance['order_by_recent']);
        $instance['order_recent'] = stripcslashes($new_instance['order_recent']);
        return $instance;
    }

    // Creating widget front-end
    function widget( $args, $instance ) {
        global $wpdb;
        extract($args);
        $title_recentnews= empty($instance['title_recentnews']) ? '' : $instance['title_recentnews'];
        $numbershow= empty($instance['numbershow']) ? '' : $instance['numbershow'];
        $order_by_recent= empty($instance['order_by_recent']) ? '' : $instance['order_by_recent'];
        $order_recent= empty($instance['order_recent']) ? '' : $instance['order_recent'];
        echo tth_esc_data($before_widget);
        if(!empty($instance['title'])){
            echo tth_esc_data($args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'], 'tags');
        }?>
        <h3 class="title text-left text-uppercase color-primary"><?php echo esc_attr($title_recentnews);?></h3>
        <div class="list-post-view">
            <ul class="tth-recent_post">
                <?php $args = array(
                'post_type'      => 'post',
                'posts_per_page' => $numbershow,
                'order'          => $order_recent,
                'orderby'        => $order_by_recent,
                );
                $i=0;
                $wp_query = new WP_Query($args);
                if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
                    $i++;
                    $url_image_large = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID(),'full') );
                ?>
                <li class="d-flex justify-content-start align-items-center">
                    <a href="<?php the_permalink();?>">
                        <img class="img-fluid" src="<?php echo esc_url($url_image_large);?>" alt="<?php echo get_the_title();?>">
                        <div class="title-view">
                            <h5>
                                <?php echo tth_title_excerpt(12);?>
                            </h5>
                        </div>
                    </a>
                </li>
                <?php endwhile;endif;?>
            </ul>
        </div>
        <?php
        echo tth_esc_data($after_widget);
    }
    // public function tth_add_widget(){
    //     if(function_exists('tth_registration_widget')) {
	//         tth_registration_widget( $this );
    //     }
    // }
    
}

add_action( 'widgets_init', 'tth_add_widget' );
 
/**
 * Register the new widget.
 *
 * @see 'widgets_init'
 */
function tth_add_widget() {
    register_widget( 'RecentNews' );
}

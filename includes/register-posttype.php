<?php
if(!class_exists('DVC_Template')) {
	class DVC_Template {
		public function __construct() {
			// add_action( 'init', [ $this, '_initCV' ], 10 );
			add_action( 'init', [ $this, '_initLocation' ], 10 );
		}
		


		public function _initLocation(){
			$register_Location = apply_filters( 'tth_apply_register_Location', true );
			if ( ! $register_Location ) {
				return false;
			}

			$labels = array(
				'name'               => __( 'Chi nhánh', 'matara' ),
				'singular_name'      => __( 'Chi nhánh', 'matara' ),
				'add_new'            => __( 'Add New Chi nhánh', 'matara' ),
				'add_new_item'       => __( 'Add New Chi nhánh', 'matara' ),
				'edit_item'          => __( 'Edit Chi nhánh', 'matara' ),
				'new_item'           => __( 'New Chi nhánh', 'matara' ),
				'view_item'          => __( 'View Chi nhánh', 'matara' ),
				'search_items'       => __( 'Search Chi nhánh', 'matara' ),
				'not_found'          => __( 'No Chi nhánh found', 'matara' ),
				'not_found_in_trash' => __( 'No Chi nhánh found in Trash', 'matara' ),
				'parent_item_colon'  => __( 'Parent Chi nhánh:', 'matara' ),
				'menu_name'          => __( 'Chi nhánh', 'matara' ),
			);
			$args   = array(
				'labels'              => $labels,
				'hierarchical'        => true,
				'description'         => 'Danh sách Chi nhánh',
				'supports'            => array( 'title', 'editor', 'thumbnail' ),
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'menu_position'       => 5,
				'menu_icon'           => 'dashicons-image-flip-horizontal',
				'show_in_nav_menus'   => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'has_archive'         => true,
				'query_var'           => true,
				'can_export'          => true,
				'capability_type'     => 'post',
				'rewrite' => array('slug' => 'cv-location', 'with_front' => false)
			);
			register_post_type( 'cv_location', $args );
		}

	}
	new DVC_Template();
}
?>
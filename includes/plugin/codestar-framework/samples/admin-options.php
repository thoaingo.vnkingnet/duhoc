<?php if ( ! defined( 'ABSPATH' )  ) { die; } // Cannot access directly.

//
// Set a unique slug-like ID
//
$prefix = 'theme_option';

//
// Create options
//
CSF::createOptions( $prefix, array(
  'menu_title' => 'Theme Options',
  'menu_slug'  => 'theme_option',
) );

//
// Create a section
//
CSF::createSection( $prefix, array(
  'title'  => 'Header',
  'icon'   => 'fas fa-rocket',
  'fields' => array(

    //
    // A text field
    //
    array(
      'id'    => 'logo',
      'type'  => 'upload',
      'title' => 'Logo',
    ),
    array(
      'id'    => 'phone-number',
      'type'  => 'text',
      'title' => 'Số điện thoại',
    ),
    

  )
) );

//
// Basic Fields
//
CSF::createSection( $prefix, array(
  'id'    => 'home_page',
  'title' => 'Home page',
  'icon'  => 'fas fa-plus-circle',
) );
//
// Field: text
//
CSF::createSection( $prefix, array(
  'parent'      => 'home_page',
  'title'       => 'Banner',
  'icon'        => 'far fa-square',
  'description' => '',
  'fields'      => array(

    array(
      'id'    => 'image-banner',
      'type'  => 'upload',
      'title' => 'Ảnh banner',
    ),

    array(
      'id'    => 'title',
      'type'  => 'textarea',
      'title' => 'Tiêu đề',
    ),

    array(
      'id'    => 'subtitle',
      'type'  => 'textarea',
      'title' => 'Nội dung content',
    ),

    array(
      'id'    => 'image-nam-toancau',
      'type'  => 'upload',
      'title' => 'Ảnh năm toàn cầu',
    ),
    array(
      'id'    => 'image-nam-taivietnam',
      'type'  => 'upload',
      'title' => 'Ảnh năm tại việt nam',
    ),

    array(
      'id'     => 'button-banner',
      'type'   => 'repeater',
      'title'  => 'List button',
      'fields' => array(
        array(
          'id'    => 'title',
          'type'  => 'text',
          'title' => 'Title button'
        ),
        array(
          'id'    => 'url',
          'type'  => 'text',
          'title' => 'Link'
        ),
      ),
    ),

  )
) );

//
// Field: textarea
//
CSF::createSection( $prefix, array(
  'parent'      => 'home_page',
  'title'       => 'Có gì đang diễn ra',
  'icon'        => 'far fa-square',
  'description' => '',
  'fields'      => array(

    array(
      'id'     => 'co-gi-dang-dien-ra',
      'type'   => 'repeater',
      'title'  => 'Có gì đang diễn ra',
      'fields' => array(
        array(
          'id'    => 'image',
          'type'  => 'upload',
          'title' => 'Image'
        ),
        array(
          'id'    => 'url',
          'type'  => 'text',
          'title' => 'Link'
        ),
      ),
    ),
  )
) );

//
// Field: select
//
CSF::createSection( $prefix, array(
  'parent'      => 'home_page',
  'title'       => 'Khóa học',
  'icon'        => 'fas fa-list',
  'description' => '',
  'fields'      => array(
    
    array(
      'id'     => 'courses_home',
      'type'   => 'repeater',
      'title'  => 'Học ở đây',
      'fields' => array(
        array(
          'id'    => 'name_course',
          'type'  => 'textarea',
          'title' => 'Tên khóa học'
        ),
        array(
          'id'    => 'image',
          'type'  => 'upload',
          'title' => 'Image'
        ),
        array(
          'id'    => 'url',
          'type'  => 'text',
          'title' => 'Xem thêm'
        ),
      ),
    ),
  )
) );

//
// Field: checkbox
//
CSF::createSection( $prefix, array(
  'parent'      => 'home_page',
  'title'       => 'Vì sao chọn',
  'icon'        => 'fas fa-check-square',
  'description' => '',
  'fields'      => array(

    array(
      'id'    => 'usp_home_title',
      'type'  => 'textarea',
      'title' => 'Title',
    ),
    array(
      'id'    => 'usp_image-banner',
      'type'  => 'upload',
      'title' => 'Ảnh banner',
    ),
    array(
      'id'    => 'usp_image',
      'type'  => 'upload',
      'title' => 'Ảnh',
    ),
    array(
      'id'    => 'usp_content',
      'type'  => 'textarea',
      'title' => 'Content',
    ),
    array(
      'id'    => 'usp_box-right',
      'type'  => 'textarea',
      'title' => 'Box right',
    ),
    array(
      'id'     => 'usp_box_list',
      'type'   => 'repeater',
      'title'  => 'Học ở đây',
      'fields' => array(
        array(
          'id'    => 'number',
          'type'  => 'text',
          'title' => 'Số'
        ),
        array(
          'id'    => 'text',
          'type'  => 'textarea',
          'title' => 'Content'
        ),
      ),
    ),

  )
) );

CSF::createSection( $prefix, array(
  'parent'      => 'home_page',
  'title'       => 'Slider video',
  'icon'        => 'fas fa-dot-circle',
  'description' => '',
  'fields'      => array(

    array(
      'id'    => 'nhatky_title',
      'type'  => 'textarea',
      'title' => 'Title',
    ),
    array(
      'id'    => 'nhatky_content',
      'type'  => 'textarea',
      'title' => 'Content',
    ),
    array(
      'id'     => 'nhatky_video',
      'type'   => 'repeater',
      'title'  => 'List video',
      'fields' => array(
        array(
          'id'    => 'url',
          'type'  => 'text',
          'title' => 'Url video youtube'
        ),
      ),
    ),

  )
) );
CSF::createSection( $prefix, array(
  'parent'      => 'home_page',
  'title'       => 'Bài viết',
  'icon'        => 'fas fa-dot-circle',
  'description' => '',
  'fields'      => array(
    array(
      'id'          => 'category_one',
      'type'        => 'select',
      'title'       => 'Chọn danh mục 1',
      'chosen'      => true,
      'ajax'        => true,
      'options'     => 'category',
      'placeholder' => 'Chọn Danh mục 1',
    ),
    array(
      'id'          => 'category_two',
      'type'        => 'select',
      'title'       => 'Chọn danh mục 2',
      'chosen'      => true,
      'ajax'        => true,
      'options'     => 'category',
      'placeholder' => 'Chọn Danh mục 2',
    ),
  )
) );
CSF::createSection( $prefix, array(
  'parent'      => 'home_page',
  'title'       => 'Đối tác',
  'icon'        => 'fas fa-dot-circle',
  'description' => '',
  'fields'      => array(

    
    array(
      'id'     => 'doitac_list',
      'type'   => 'repeater',
      'title'  => 'List video',
      'fields' => array(
        array(
          'id'    => 'image',
          'type'  => 'upload',
          'title' => 'Image'
        ),
        array(
          'id'    => 'url',
          'type'  => 'text',
          'title' => 'Url'
        ),
      ),
    ),

  )
) );
CSF::createSection( $prefix, array(
  'id'    => 'footer',
  'title' => 'Footer',
  'icon'  => 'fas fa-plus-circle',
) );
CSF::createSection( $prefix, array(
  'parent'      => 'footer',
  'title'       => 'Footer',
  'icon'        => 'far fa-square',
  'description' => '',
  'fields'      => array(
    array(
      'id'    => 'linkfacebook',
      'type'  => 'text',
      'title' => 'Link facebook',
    ),
    array(
      'id'    => 'linkyoutube',
      'type'  => 'text',
      'title' => 'Link youtube',
    ),
    array(
      'id'    => 'linkinsta',
      'type'  => 'text',
      'title' => 'Link Instagram',
    ),
    array(
      'id'    => 'linkmail',
      'type'  => 'text',
      'title' => 'Mail',
    ),
    array(
      'id'    => 'copyright',
      'type'  => 'textarea',
      'title' => 'Copy right',
    ),

  )
) );
//advantion
CSF::createSection( $prefix, array(
  'id'    => 'sidebar_book',
  'title' => 'Sidebar Box',
  'icon'  => 'fas fa-plus-circle',
) );
CSF::createSection( $prefix, array(
  'parent'      => 'sidebar_book',
  'title'       => 'Setting top box',
  'icon'        => 'far fa-square',
  'description' => '',
  'fields'      => array(
    array(
      'id'    => 'ccv_facebook',
      'type'  => 'text',
      'title' => 'Link facebook',
    ),
    array(
      'id'    => 'ccv_phone',
      'type'  => 'text',
      'title' => 'Number phone',
    ),
    array(
      'id'    => 'ccv-download',
      'type'  => 'text',
      'title' => 'Link download',
    ),
    array(
      'id'    => 'ccv-gmail',
      'type'  => 'text',
      'title' => 'Mail',
    ),

  )
) );
//
// Field: backup
//
CSF::createSection( $prefix, array(
  'title'       => 'Backup',
  'icon'        => 'fas fa-shield-alt',
  'description' => 'Visit documentation for more details on this field: <a href="http://codestarframework.com/documentation/#/fields?id=backup" target="_blank">Field: backup</a>',
  'fields'      => array(

    array(
      'type' => 'backup',
    ),

  )
) );

//
// Others
//
CSF::createSection( $prefix, array(
  'title'       => 'Others',
  'icon'        => 'fas fa-bolt',
  'description' => 'Visit documentation for more details: <a href="http://codestarframework.com/documentation/#/fields?id=others" target="_blank">Others</a>',
  'fields'      => array(

    array(
      'type'    => 'heading',
      'content' => 'This is a heading field',
    ),

    array(
      'type'    => 'subheading',
      'content' => 'This is a subheading field',
    ),

    array(
      'type'    => 'content',
      'content' => 'This is a content field',
    ),

    array(
      'type'    => 'submessage',
      'style'   => 'success',
      'content' => 'This is a <strong>submessage</strong> field. And using style <strong>success</strong>',
    ),

    array(
      'type'    => 'content',
      'content' => 'This is a content field',
    ),

    array(
      'type'    => 'submessage',
      'style'   => 'info',
      'content' => 'This is a <strong>submessage</strong> field. And using style <strong>info</strong>',
    ),

    array(
      'type'    => 'submessage',
      'style'   => 'warning',
      'content' => 'This is a <strong>submessage</strong> field. And using style <strong>warning</strong>',
    ),

    array(
      'type'    => 'submessage',
      'style'   => 'danger',
      'content' => 'This is a <strong>submessage</strong> field. And using style <strong>danger</strong>',
    ),

    array(
      'type'    => 'notice',
      'style'   => 'success',
      'content' => 'This is a <strong>notice</strong> field. And using style <strong>success</strong>',
    ),

    array(
      'type'    => 'notice',
      'style'   => 'info',
      'content' => 'This is a <strong>notice</strong> field. And using style <strong>info</strong>',
    ),

    array(
      'type'    => 'notice',
      'style'   => 'warning',
      'content' => 'This is a <strong>notice</strong> field. And using style <strong>warning</strong>',
    ),

    array(
      'type'    => 'notice',
      'style'   => 'danger',
      'content' => 'This is a <strong>notice</strong> field. And using style <strong>danger</strong>',
    ),

    array(
      'type'    => 'content',
      'content' => 'This is a <strong>content</strong> field. You can write some contents here.',
    ),

  )
) );

<?php
    get_header();
    $mtr_posts_per_page = get_option('posts_per_page',true);
    ?>
    <section class="container" style="margin-top:40px">
        <div class="row flex-column-reverse flex-md-row margin-top-24">
            <div class="col-12">
                <h3 class="text-left title-archive text-uppercase color-primary"> <?php echo get_queried_object()->name;?></h3>
            </div>
            
        </div>
        <div class="row list-tiem margin-top-24">
            <div class="col-sm-8 col-12">
                <?php
                if ( have_posts() ) {
                    while ( have_posts() ) :
                        the_post();
                        echo mtr_load_template( 'blog/content', '', ['posts_per_page' => $mtr_posts_per_page]);
                    endwhile;
                }
                wp_reset_postdata();
                ?>
                <?php echo tth_pagination();?>
            
            </div>
            <div class="col-sm-4 col-12">
                <div class="sidebar-ccv">
                    <?php dynamic_sidebar( 'blog-sidebar' ) ?>
                </div>
                
            </div>
        </div>
    </section>
<?php get_footer();
?>